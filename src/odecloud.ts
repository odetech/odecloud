import {
  createAuthorizationHeader, getRequestOptions, authentication,
} from './helpers';
import {
  articles, publications, events, networks, users, comments, tags, notifications, bookmarks, files,
  auth, surveys, questions, answers, notificationSettings, feed, websocket, tasks, orgs, projects, clientContacts,
  notes, searches, timeTrackers, skillsets, masterNotifications, avatars, appStores, emails, messages, links,
  expertises, hashtags, externals, questionnaires, nspLogs, themes, globals,
} from './api';

class OdeCloud {
  _authToken = "";
  _endpoint = "";
  userId = "";

  constructor(data: any) {
    this._authToken = "";
    this._endpoint = "";
    this.userId = "";

    if ("endpoint" in data) {
      this._endpoint = data["endpoint"];
    } else {
      throw Error(
        "'endPoint' is required. Example: endpoint='https://odecloud.app/api/v1"
      );
    }
  }

  _getRequestOptions = (searchParams: any, headers?: any) =>
    getRequestOptions(
      searchParams,
      this._authToken,
      createAuthorizationHeader,
      this.userId,
      headers?.token,
      headers?.app
    );

  getEndpoint = (requestAPi: string) => {
    return `${this._endpoint}/${requestAPi}`;
  };

  setAuthToken = (authToken: string) => {
    this._authToken = authToken;
  };

  setUserId = (userId: string) => {
    this.userId = userId;
  };

  _updateTimeZoneIfIsEmpty = async (userId: string) => {
    const user = await this.users.getUser(userId);
    if (!user.profile?.timezone?.offset) {
      // eslint-disable-next-line new-cap
      const timezoneName = Intl.DateTimeFormat().resolvedOptions().timeZone;
      await this.users.patchTimeZone(userId, {
        timezone: {
          timezoneName,
          offset: new Date().getTimezoneOffset(),
          // eslint-disable-next-line new-cap
          gmtFormat: Intl.DateTimeFormat("en", {
            timeZoneName: "short",
            timeZone: timezoneName,
          }).formatToParts()[6].value,
        },
      });
    }
    return user;
  };

  // TODO implement oauth for @odecloud/odecloud
  resync = async ({
    authToken,
    userId,
    clientKey,
    clientSecret,
    url,
    interval,
    isCheckTimeZone,
  }: {
    authToken?: string;
    userId?: string;
    clientKey?: string;
    clientSecret?: string;
    url: string;
    interval?: number;
    isCheckTimeZone?: boolean;
  }) => {
    if (authToken && userId && url) {
      const payload: {
        authToken?: string;
        userId?: string;
        clientKey?: string;
        clientSecret?: string;
        url: string;
        interval?: number;
      } = {
        url,
      };

      if (clientKey && clientSecret) {
        payload.clientKey = clientKey;
        payload.clientSecret = clientSecret;
      } else {
        payload.userId = userId;
        payload.authToken = authToken;
      }
      payload.interval = interval;

      let user = null;
      if (isCheckTimeZone) {
        user = await this._updateTimeZoneIfIsEmpty(userId);
      }

      await this.auth.resync(payload);

      this._authToken = authToken;
      this.userId = userId;
      return user;
    } else {
      throw Error(
        "Either userId and authToken are required or clientKey and clientSecret are required."
      );
    }
  };

  authenticate = (credentials: any) =>
    authentication(
      credentials,
      this.getEndpoint,
      this.setAuthToken,
      this.setUserId
    );

  /**************************
   ****** Articles API ******
   **************************/

  articles = { ...articles(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Publications API ***
   **************************/

  publications = { ...publications(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Events API ***
   **************************/

  events = { ...events(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Networks API ***
   **************************/

  networks = { ...networks(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Users API ***
   **************************/

  users = { ...users(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Noti Logs API ***
   **************************/

  nspLogs = { ...nspLogs(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Comments API ***
   **************************/

  comments = { ...comments(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Tags API ***
   **************************/

  tags = { ...tags(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Notifications API ***
   **************************/

  notifications = {
    ...notifications(this._getRequestOptions, this.getEndpoint),
  };

  /**************************
   ***** Bookmarks API ***
   **************************/

  bookmarks = { ...bookmarks(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Files API ***
   **************************/

  files = { ...files(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Auth API ***
   **************************/

  auth = { ...auth(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Surveys API ***
   **************************/

  surveys = { ...surveys(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Questions API ***
   **************************/

  questions = { ...questions(this._getRequestOptions, this.getEndpoint) };

  /**************************
   ***** Answers API ***
   **************************/

  answers = { ...answers(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Notification Settings API ***
   **************************/

  notificationSettings = {
    ...notificationSettings(this._getRequestOptions, this.getEndpoint),
  };

  /**************************
   *** Feed API ***
   **************************/

  feed = { ...feed(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** WEBSOCKET ***
   **************************/

  websocket = { ...websocket(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Tasks API ***
   **************************/

  tasks = { ...tasks(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Orgs API ***
   **************************/

  orgs = { ...orgs(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Projects API ***
   **************************/

  projects = { ...projects(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Client Contact API ***
   **************************/

  clientContact = {
    ...clientContacts(this._getRequestOptions, this.getEndpoint),
  };

  /**************************
   *** Notes API ***
   **************************/

  notes = { ...notes(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Searches API ***
   **************************/

  searches = { ...searches(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Time Trackers API ***
   **************************/

  timeTrackers = { ...timeTrackers(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Skillsets API ***
   **************************/

  skillsets = { ...skillsets(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Master Notifications API ***
   **************************/

  masterNotifications = {
    ...masterNotifications(this._getRequestOptions, this.getEndpoint),
  };

  /**************************
   *** Avatars API ***
   **************************/

  avatars = { ...avatars(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** App Stores API ***
   **************************/

  appStores = { ...appStores(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Emails API ***
   **************************/

  emails = { ...emails(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Messages API ***
   **************************/

  messages = { ...messages(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Links API ***
   **************************/

  links = { ...links(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Hashtags API ***
   **************************/

  hashtags = { ...hashtags(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Expertises API ***
   **************************/

  expertises = { ...expertises(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Externals API ***
   **************************/

  externals = { ...externals(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Questionnaires API ***
   **************************/

  questionnaires = {
    ...questionnaires(this._getRequestOptions, this.getEndpoint),
  };

  themes = { ...themes(this._getRequestOptions, this.getEndpoint) };

  /**************************
   *** Globals API ***
   **************************/

  globals = { ...globals(this._getRequestOptions, this.getEndpoint) };
}

export default OdeCloud;
