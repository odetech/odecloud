import { requests } from './requests';
import { TimeTracker } from './model';
import type TimeTrackerTypes from './types';

const timeTrackers = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: TimeTracker,
    payload: { ...new TimeTracker({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  TimeTrackerTypes,
};

export default timeTrackers;
