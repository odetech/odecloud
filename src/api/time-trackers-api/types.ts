type TimeTrackerTypes = {
  _id: string,
  trackedBy: string,
  taskId: string,
  times?: {
    startedAt?: number,
    endedAt?: number,
  }[],
  preset?: number,
  accumulative?: number,
  data?: any,
};

export default TimeTrackerTypes;
