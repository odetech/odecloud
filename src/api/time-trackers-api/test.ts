import { expect } from 'chai';
import { TimeTracker } from './model';

describe('TimeTracker\'s Model', () => {
  const MockTimeTracker = () => new TimeTracker({
    _id: '123456',
    trackedBy: '987',
    taskId: '876',
    preset: 1,
  });

  it('checks basic', () => {
    const timeTracker = MockTimeTracker();
    timeTracker.setCurrentUserId('YYY')
    expect(timeTracker._id).to.equal('123456');
    expect(timeTracker.trackedBy).to.equal('987');
    expect(timeTracker.taskId).to.equal('876');
    expect(timeTracker.preset).to.equal(1);
  });

  describe('payload()', () => {
    const timeTracker = MockTimeTracker();
    timeTracker.setCurrentUserId('YYY');

    it('checks getPostPayload()', () => {
      const payload = timeTracker.payload().getPostPayload({
        trackedBy: 'qwe',
        preset: 8,
      });
      expect(payload.trackedBy).to.equal('qwe');
      expect(payload.taskId).to.equal(timeTracker.taskId);
      expect(payload.preset).to.equal(8);
    });

    it('checks getPatchPayload()', () => {
      const payload = timeTracker.payload().getPatchPayload({
        trackedBy: 'asd',
        accumulative: 5,
        preset: 7,
      });
      expect(payload.trackedBy).to.equal('asd');
      expect(payload.accumulative).to.equal(5);
      expect(payload.preset).to.equal(7);
    });

  });
});
