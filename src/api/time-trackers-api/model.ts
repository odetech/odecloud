import { BasicModel } from '../basicModel';
import TimeTrackerTypes from './types';

export class TimeTracker extends BasicModel implements TimeTrackerTypes {
  _id = '';
  trackedBy = '';
  taskId = '';
  preset = 0;

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload?: any) => {
      const payload: {
        trackedBy: string,
        taskId: string,
        preset?: number,
        createdBy?: string,
        updatedBy?: string,
        createdAt?: Date,
        updatedAt?: Date,
        data?: string[],
      } = {
        trackedBy: postPayload.trackedBy || this.trackedBy,
        taskId: postPayload.taskId || this.taskId,
      };

      const keys = Object.keys(postPayload);
      keys.forEach((key: string) => {
        if (postPayload[key] !== undefined) {
          payload[key] = postPayload[key];
        }
      });

      return payload;
    },
    getPatchPayload: (patchPayload?: any) => {
      const payload: {
        trackedBy?: string,
        taskId?: string,
        times?: {
          startedAt?: number,
          endedAt?: number,
        }[],
        preset?: number,
        accumulative?: number,
        data?: string[],
        createdBy?: string,
        updatedBy: string,
        createdAt?: Date,
        updatedAt?: Date,
      } = {
        updatedBy: this.updatedBy,
      };

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
  });
}
