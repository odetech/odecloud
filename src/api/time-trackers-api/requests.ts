import {deleteRequest, get, patch, post} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getTimeTrackers: async (searchParams?: {
    trackedBy?: string,
    data?: string[],
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint('time-trackers');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getTimeTracker: async (timeTrackerId: string, searchParams?: {
    trackedBy?: string,
    data?: string[],
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint(`time-trackers/${timeTrackerId}`);
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint('time-trackers'),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (timeTrackerId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`time-trackers/${timeTrackerId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patchEdit: async (timeTrackerId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`time-trackers/${timeTrackerId}/edit`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (timeTrackerId: string) => {
    try {
      const resultId = await deleteRequest(
        getRequestOptions,
        getEndpoint(`time-trackers/${timeTrackerId}`),
      );
      return resultId;
    } catch(e) {
      // handle error
      throw e;
    }
  },
  getReports: async (searchParams?: {
    orgId?: string,
    createdAtStart?: string,
    createdAtEnd?: string,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('time-trackers/reports'),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getReportTotalTime: async (searchParams?: {
    orgId?: string,
    createdAtStart?: string,
    createdAtEnd?: string,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('time-trackers/reports-total-time'),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  downloadReports: async (searchParams) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`time-trackers/download-reports`),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  uploadCsv: async (payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint('time-trackers/upload-csv'),
        payload,
        'formData',
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
