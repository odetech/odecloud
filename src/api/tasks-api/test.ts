import { expect } from 'chai';
import { Task } from './model';

describe('Task\'s Model', () => {
  const MockTask = () => new Task({
    _id: '123456',
    title: 'Hello World',
    description: 'qwe',
    gigId: 'asd',
    orgId: 'zxc',
    projectId: 'wer',
    assignedToUserId: 'sdf',
    activity: 1,
    priority: 5,
    data: {
      commentsCount: 2,
    },
    tags: ['xcv'],
    subTaskId: 'xcv',
  });

  it('checks basic', () => {
    const task = MockTask();

    expect(task._id).to.equal('123456');
    expect(task.title).to.equal('Hello World');
    expect(task.description).to.equal('qwe');
    expect(task.gigId).to.equal('asd');
    expect(task.orgId).to.equal('zxc');
    expect(task.projectId).to.equal('wer');
    expect(task.assignedToUserId).to.equal('sdf');
    expect(task.activity).to.equal(1);
    expect(task.priority).to.equal(5);
    expect(task.data.commentsCount).to.equal(2);
    expect(task.tags[0]).to.equal('xcv');
    expect(task.subTaskId).to.equal('xcv');
  });

  describe('payload()', () => {
    const task = MockTask();

    it('checks getPostPayload()', () => {
      const payload = task.payload().getPostPayload({
        title: 'qwe',
        projectId: 'asd',
        updatedBy: 'zxc',
        createdBy: 'wer',
      });

      expect(payload.title).to.equal('qwe');
      expect(payload.projectId).to.equal('asd');
      expect(payload.updatedBy).to.equal('zxc');
      expect(payload.createdBy).to.equal('wer');
    });

    it('checks getPatchPayload()', () => {
      const payload = task.payload().getPatchPayload({
        projectId: 'qwe',
        updatedBy: 'asd',
      });

      expect(payload.projectId).to.equal('qwe');
      expect(payload.updatedBy).to.equal('asd');
    });

    it('checks getPutPayload()', () => {
      const payload = task.payload().getPutPayload({
        projectId: 'qwe',
        updatedBy: 'asd',
      });

      expect(payload.projectId).to.equal('qwe');
      expect(payload.updatedBy).to.equal('asd');
    });

  });
});
