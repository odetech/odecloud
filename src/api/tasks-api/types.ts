type TaskTypes = {
  _id: string,
  title?: string,
  description?: string,
  gigId?: string,
  orgId?: string,
  projectId: string,
  assignedToUserId?: string,
  activity?: number,
  priority?: number,
  data?: any,
  tags?: string[],
  subTaskId?: string,
  additionalFields?: any,
  dueDate?: Date,
};

export default TaskTypes;
