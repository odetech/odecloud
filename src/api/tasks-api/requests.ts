import {
  deleteRequest,
  get, patch, post, put,
} from '../../base-requests';

const getParams = (searchParams: any) => {
  const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
  if (searchParamsCopy.data) {
    delete searchParamsCopy.data;
  }
  if (searchParamsCopy.projectId) {
    delete searchParamsCopy.projectId;
  }
  if (searchParamsCopy._id) {
    delete searchParamsCopy._id;
  }
  if (searchParamsCopy.hashtags) {
    delete searchParamsCopy.hashtags;
  }
  const searchParamsString = new URLSearchParams(searchParamsCopy);
  if (searchParams?.data) {
    searchParams.data.forEach((item) => {
      searchParamsString.append('data', item);
    });
  }
  if (searchParams?.hashtags) {
    searchParams.hashtags.forEach((item) => {
      searchParamsString.append('hashtags', item);
    });
  }
  if (searchParams?.projectId) {
    searchParams.projectId.forEach((item) => {
      searchParamsString.append('projectId', item);
    });
  }
  if (searchParams?._id) {
    searchParams._id.forEach((item) => {
      searchParamsString.append('_id', item);
    });
  }
  return searchParamsString;
}

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getConversation: async (associatedId: string, searchParams: { limit: number }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`tasks/conversation/${associatedId}?limit=${searchParams.limit}`),
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getTasks: async (searchParams?: {
    orgId?: string,
    projectId?: string[],
    activity?: number,
    priority?: number,
    createdBy?: string,
    data?: string[],
    hashtags?: string[],
    isActive?: boolean,
    pageNumber?: number,
    limit?: number,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
    orderBy?: string,
    sortBy?: string,
    _id?: string[],
  }) => {
    const searchParamsString = getParams(searchParams);

    let url = getEndpoint('tasks');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  exportTasks: async (searchParams?: {
    orgId?: string,
    projectId?: string[],
    activity?: number,
    priority?: number,
    createdBy?: string,
    data?: string[],
    hashtags?: string[],
    isActive?: boolean,
    pageNumber?: number,
    limit?: number,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
    orderBy?: string,
    sortBy?: string,
    _id: string[],
  }, payload?: { fields: string[] }) => {
    const searchParamsString = getParams(searchParams);

    let url = getEndpoint('tasks');
    if (searchParams) {
      url = url.concat(`/export?${searchParamsString.toString()}`);
    }
    try {
      const result = await post(
        getRequestOptions,
        url,
        payload,
      );
      return result;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getTask: async (taskId: string, searchParams?: {
    orgId?: string,
    projectId?: string,
    activity?: number,
    priority?: number,
    createdBy?: string,
    data?: string[],
    isActive?: boolean,
    pageNumber?: number,
    limit?: number,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
    orderBy?: string,
    sortBy?: string,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint(`tasks/${taskId}`);
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getTasksMetrics: async (taskId: string, searchParams?: {
    limit?: number,
    orgId?: string,
    projectId?: string,
    assignedToUserId?: string,
    isActive?: boolean,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('tasks/metrics'),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint('tasks'),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  put: async (taskId: string, payload: any) => {
    try {
      const request = await put(
        getRequestOptions,
        getEndpoint(`tasks/${taskId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (taskId: string) => {
    try {
      const resultId = await deleteRequest(
        getRequestOptions,
        getEndpoint(`tasks/${taskId}`),
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (taskId: string, payload: any) => {
    try {
      const resultId = await patch(
        getRequestOptions,
        getEndpoint(`tasks/${taskId}`),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  search: async (searchParams: {
    search: string,
    orgId?: string,
    projectId?: string,
    createdAt?: string,
    createdAtStart?: string,
    createdAtEnd?: string,
    dueDate?: string,
    dueDateStart?: string,
    dueDateEnd?: string,
    activity?: number,
    priority?: number,
    createdBy?: string,
    data?: string[],
    pageNumber?: number,
    limit?: number,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
    orderBy?: string,
    sortBy?: string,
    assignedToUserId?: string,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint('tasks/search');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
