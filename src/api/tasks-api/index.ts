import { requests } from './requests';
import { Task } from './model';
import type TaskTypes from './types';

const tasks = (getRequestOptions: any, getEndpoint: any) => ({
  model: Task,
  payload: { ...new Task({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  TaskTypes,
};

export default tasks;
