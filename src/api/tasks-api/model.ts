import { BasicModel } from '../basicModel';
import TaskTypes from './types';

export class Task extends BasicModel implements TaskTypes {
  _id = '';
  title = '';
  description = '';
  gigId = '';
  orgId = '';
  projectId = '';
  assignedToUserId = '';
  activity = 3;
  priority = 5;
  data = null;
  tags = [];
  subTaskId = '';
  additionalFields = null;
  dueDate = new Date();

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload: any) => {
      const payload: {
        title: string,
        description?: string,
        gigId?: string,
        orgId?: string,
        projectId: string,
        assignedToUserId?: string,
        activity?: number,
        priority?: number,
        data?: string[],
        tags?: string[],
        subTaskId?: string,
        additionalFields?: any,
        dueDate?: Date,
        updatedBy: string,
        createdBy: string,
      } = {
        title: '',
        projectId: '',
        updatedBy: '',
        createdBy: '',
      };

      const keys = Object.keys(postPayload);
      keys.forEach((key: string) => {
        if (postPayload[key] !== undefined) {
          payload[key] = postPayload[key];
        }
      });

      return payload;
    },
    getPatchPayload: (patchPayload: any) => {
      const payload: {
        title?: string,
        description?: string,
        gigId?: string,
        orgId?: string,
        projectId: string,
        assignedToUserId?: string,
        activity?: number,
        priority?: number,
        data?: string[],
        tags?: string[],
        subTaskId?: string,
        additionalFields?: any,
        dueDate?: Date,
        updatedBy: string,
      } = {
        projectId: '',
        updatedBy: '',
      };

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
    getPutPayload: (putPayload: any) => {
      const payload: {
        title?: string,
        description?: string,
        gigId?: string,
        orgId?: string,
        projectId: string,
        assignedToUserId?: string,
        activity?: number,
        priority?: number,
        data?: any,
        tags?: string[],
        subTaskId?: string,
        additionalFields?: any,
        dueDate?: Date,
        updatedBy: string,
      } = {
        projectId: '',
        updatedBy: '',
      };

      const keys = Object.keys(putPayload);
      keys.forEach((key: string) => {
        if (putPayload[key] !== undefined) {
          payload[key] = putPayload[key];
        }
      });

      return payload;
    },
  });
}
