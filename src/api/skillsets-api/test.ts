import { expect } from 'chai';
import { Skillset } from './model';

describe('Skillset\'s Model', () => {
  const MockSkillset = () => new Skillset({
    _id: '123456',
    values: ['you'],
    userId: 'sdf',
  });

  it('checks basic', () => {
    const skillset = MockSkillset();
    skillset.setCurrentUserId('YYY')
    expect(skillset._id).to.equal('123456');
    expect(skillset.values[0]).to.equal('you');
    expect(skillset.userId).to.equal('sdf');
  });

  describe('payload()', () => {
    const skillset = MockSkillset();
    skillset.setCurrentUserId('YYY');

    it('checks getPostPayload()', () => {
      const payload = skillset.payload().getPostPayload({
        values: ['asd', 'qwe'],
        userId: 'zxc',
        createdAt: new Date(),
        updatedAt: new Date(),
      });
      expect(payload.values[1]).to.equal('qwe');
      expect(payload.userId).to.equal('zxc');
      expect(new Date(payload.createdAt).getTime() > new Date(skillset.createdAt).getTime()).to.equal(true);
      expect(new Date(payload.updatedAt).getTime() > new Date(skillset.updatedAt).getTime()).to.equal(true);
    });

    it('checks getPatchPayload()', () => {
      const payload = skillset.payload().getPatchPayload({
        values: ['dfg', 'asd'],
        userId: 'cvb',
      });
      expect(payload.values[0]).to.equal('dfg');
      expect(payload.userId).to.equal('cvb');
    });

  });
});
