import { BasicModel } from '../basicModel';
import SkillsetTypes from './types';

export class Skillset extends BasicModel implements SkillsetTypes {
  _id = '';
  values = [];
  userId = '';

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload?: any) => {
      const payload: {
        values?: string[],
        userId: string,
        createdBy?: string,
        updatedBy?: string,
        createdAt?: Date,
        updatedAt?: Date,
      } = {
        userId: postPayload.userId || this.userId,
      };

      const keys = Object.keys(postPayload);
      keys.forEach((key: string) => {
        if (postPayload[key] !== undefined) {
          payload[key] = postPayload[key];
        }
      });

      return payload;
    },
    getPatchPayload: (patchPayload?: any) => {
      const payload: {
        values?: string[],
        userId?: string,
        updatedAt?: Date,
      } = {};

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
  });
}
