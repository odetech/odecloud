type TagTypes = {
  _id: string,
  values?: string[],
  userId?: string,
};

export default TagTypes;
