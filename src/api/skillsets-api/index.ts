import { requests } from './requests';
import { Skillset } from './model';
import type SkillsetTypes from './types';

const skillsets = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Skillset,
    payload: { ...new Skillset({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  SkillsetTypes,
};

export default skillsets;
