import { get, patch, post } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getSkillsets: async (searchParams?: {
    userId?: string,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('skillsets'),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getSkillset: async (skillsetId: string, searchParams?: {
    userId?: string,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`skillsets/${skillsetId}`),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint('skillsets'),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (skillsetId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`skillsets/${skillsetId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
