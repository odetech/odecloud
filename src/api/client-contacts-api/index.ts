import { requests } from './requests';
import { ClientContact } from './model';
import type ClientContactTypes from './types';

const clientContacts = (getRequestOptions: any, getEndpoint: any) => ({
  model: ClientContact,
  payload: { ...new ClientContact({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  ClientContactTypes,
};

export default clientContacts;
