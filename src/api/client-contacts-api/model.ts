import ClientContactTypes from './types';

export class ClientContact implements ClientContactTypes {
  _id = '';
  associatedId = '';
  memberId = '';
  memberEmail = '';
  memberInfo = {
    firstName: '',
    lastName: '',
    position: '',
    description: '',
  };
  createdAt = '';
  updatedAt = '';

  constructor(data: any) {
    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload: any) => {
      const payload: {
        associatedId: string,
        memberId?: string,
        memberEmail: string,
        memberInfo?: {
          firstName: string,
          lastName: string,
          position: string,
          description: string,
        },
        createdAt?: string,
        updatedAt?: string,
      } = {
        associatedId: '',
        memberEmail: '',
      };

      const keys = Object.keys(postPayload);
      keys.forEach((key: string) => {
        if (postPayload[key] !== undefined) {
          payload[key] = postPayload[key];
        }
      });

      return payload;
    },
    getPatchPayload: (patchPayload: any) => {
      const payload: {
        memberEmail: string,
        memberInfo?: {
          firstName: string,
          lastName: string,
          position: string,
          description: string,
        },
        updatedAt?: string,
      } = {
        memberEmail: '',
      };

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
  });
}
