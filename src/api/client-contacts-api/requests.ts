import {
  get, patch, post, deleteRequest,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getClientContacts: async (searchParams?: {
    associatedId?: string,
    memberEmail?: string,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('client-contacts'),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getClientContact: async (clientContactId: string, searchParams?: {
    associatedId?: string,
    memberEmail?: string,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`client-contacts/${clientContactId}`),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint('client-contacts'),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (clientContactId: string) => {
    try {
      const request = await deleteRequest(
        getRequestOptions,
        getEndpoint(`client-contacts/${clientContactId}`),
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (clientContactId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`client-contacts/${clientContactId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
