type ClientContactTypes = {
  _id: string,
  associatedId: string,
  memberId?: string,
  memberEmail: string,
  memberInfo?: {
    firstName: string,
    lastName: string,
    position: string,
    description: string,
  },
  createdAt?: string,
  updatedAt?: string,
};

export default ClientContactTypes;
