import { expect } from 'chai';
import { ClientContact } from './model';

describe('ClientContact\'s Model', () => {
  const MockClientContact = () => new ClientContact({
    _id: '123456',
    associatedId: 'Hello World',
    memberId: 'qwe',
    memberEmail: 'asd',
    memberInfo: {
      firstName: 'qwe',
      lastName: 'asd',
      position: 'zxc',
      description: 'wer',
    },
    createdAt: 'dfg',
    updatedAt: 'sdf',
  });

  it('checks basic', () => {
    const clientContact = MockClientContact();

    expect(clientContact._id).to.equal('123456');
    expect(clientContact.associatedId).to.equal('Hello World');
    expect(clientContact.memberId).to.equal('qwe');
    expect(clientContact.memberEmail).to.equal('asd');
    expect(clientContact.memberInfo.firstName).to.equal('qwe');
    expect(clientContact.memberInfo.lastName).to.equal('asd');
    expect(clientContact.memberInfo.position).to.equal('zxc');
    expect(clientContact.memberInfo.description).to.equal('wer');
    expect(clientContact.createdAt).to.equal('dfg');
    expect(clientContact.updatedAt).to.equal('sdf');
  });

  describe('payload()', () => {
    const clientContact = MockClientContact();

    it('checks getPostPayload()', () => {
      const payload = clientContact.payload().getPostPayload({
        associatedId: 'wer',
        memberId: 'sdf',
        memberEmail: 'xcv',
        memberInfo: {
          firstName: 'ert',
          lastName: 'dfg',
          position: 'cvb',
          description: 'xcv',
        },
        createdAt: 'zxc',
        updatedAt: 'xcv',
      });

      expect(payload.associatedId).to.equal('wer');
      expect(payload.memberId).to.equal('sdf');
      expect(payload.memberEmail).to.equal('xcv');
      expect(payload.memberInfo.firstName).to.equal('ert');
      expect(payload.memberInfo.lastName).to.equal('dfg');
      expect(payload.memberInfo.position).to.equal('cvb');
      expect(payload.memberInfo.description).to.equal('xcv');
      expect(payload.createdAt).to.equal('zxc');
      expect(payload.updatedAt).to.equal('xcv');
    });

    it('checks getPatchPayload()', () => {
      const payload = clientContact.payload().getPatchPayload({
        memberEmail: 'sdf',
        memberInfo: {
          firstName: 'xcv',
          lastName: 'wer',
          position: 'sdf',
          description: 'asd',
        },
        updatedAt: 'cvb',
      });

      expect(payload.memberEmail).to.equal('sdf');
      expect(payload.memberInfo.firstName).to.equal('xcv');
      expect(payload.memberInfo.lastName).to.equal('wer');
      expect(payload.memberInfo.position).to.equal('sdf');
      expect(payload.memberInfo.description).to.equal('asd');
      expect(payload.updatedAt).to.equal('cvb');
    });

  });
});
