import { get, patch, post } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getNotifications: async (searchParams?: {
    data?: string[],
    recipientUserId?: string,
    isRead?: boolean,
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
      if (searchParamsCopy.data) {
        delete searchParamsCopy.data;
      }
      const searchParamsString = new URLSearchParams(searchParamsCopy);

      if (searchParams?.data) {
        searchParams.data.forEach((item) => {
          searchParamsString.append('data', item);
        });
      }

      let url = getEndpoint('notifications');
      if (searchParams) {
        url = url.concat(`/?${searchParamsString.toString()}`);
      }

      const notifications = await get(
        getRequestOptions,
        url,
      );
      return notifications;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getNotification: async (notificationId: string, searchParams?: {
    data?: string[],
    recipientUserId?: string,
    isRead?: boolean,
    limit?: number,
    isStaff?: number,
    isDev?: number,
    origin?: string | 'odesocial' | 'odetask' | 'chat',
  }) => {
    try {
      const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
      if (searchParamsCopy.data) {
        delete searchParamsCopy.data;
      }
      const searchParamsString = new URLSearchParams(searchParamsCopy);

      if (searchParams?.data) {
        searchParams.data.forEach((item) => {
          searchParamsString.append('data', item);
        });
      }

      let url = getEndpoint(`notifications/${notificationId}`);
      if (searchParams) {
        url = url.concat(`/?${searchParamsString.toString()}`);
      }
      const notification = await get(
        getRequestOptions,
        url,
      );
      return notification;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (
    notificationId: string,
    payload: any,
    additionalParams: {
      origin: string | 'odesocial' | 'odetask' | 'chat',
    },
  ) => {
    try {
      let url = getEndpoint(`notifications/${notificationId}`);
      if (additionalParams) {
        const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
        const searchParamsString = new URLSearchParams(searchParamsCopy);

        url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
      }

      const resultId = await patch(
        getRequestOptions,
        url,
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
  post: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('notifications'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
  readAll: async (payload: {
    app: string | 'odesocial' | 'odetask' | 'chat',
  }) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('notifications/read-all'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
});
