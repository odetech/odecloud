import { expect } from 'chai';
import { Notification } from './model';

describe('Notification\'s Model', () => {
  const MockNotification = () => new Notification({
    _id: '123456',
    title: 'Hello World',
    content: 'Lorem ipsum dolor sit amet',
    url: '/articles/xyz',
    fullUrl: 'https://feed.odecloud.app/articles/xyz',
    recipientUserId: 'xyz',
    isRead: false,
  });

  it('checks basic', () => {
    const notification = MockNotification();

    expect(notification._id).to.equal('123456');
    expect(notification.title).to.equal('Hello World');
    expect(notification.content).to.equal('Lorem ipsum dolor sit amet');
    expect(notification.url).to.equal('/articles/xyz');
    expect(notification.fullUrl).to.equal('https://feed.odecloud.app/articles/xyz');
    expect(notification.recipientUserId).to.equal('xyz');
    expect(notification.isRead).to.equal(false);
  });

  describe('payload()', () => {
    const notification = MockNotification();

    it('checks getPostPayload()', () => {
      const payload = notification.payload().getPostPayload();

      expect(payload.title).to.equal(notification.title);
      expect(payload.content).to.equal(notification.content);
      expect(payload.url).to.equal(notification.url);
      expect(payload.fullUrl).to.equal(notification.fullUrl);
      expect(payload.recipientUserId).to.equal(notification.recipientUserId);
      expect(payload.isRead).to.equal(notification.isRead);
      expect(new Date(payload.createdAt).getTime() > new Date(notification.createdAt).getTime()).to.equal(true);
      expect(new Date(payload.updatedAt).getTime() > new Date(notification.updatedAt).getTime()).to.equal(true);
    });

    it('checks getPatchPayload()', () => {
      const payload = notification.payload().getPostPayload();

      expect(payload.isRead).to.equal(notification.isRead);
      expect(new Date(payload.updatedAt).getTime() > new Date(notification.updatedAt).getTime()).to.equal(true);
    });
  });
});
