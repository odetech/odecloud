type NotificationTypes = {
  _id: string,
  title: string,
  content: string,
  url: string, // short relative url
  fullUrl: string, // absolute url
  recipientUserId: string,
  isRead: boolean,
};

export default NotificationTypes;
