import { BasicModel } from '../basicModel';
import NotificationTypes from './types';

export class Notification extends BasicModel implements NotificationTypes {
  _id = '';
  title = '';
  content = '';
  url = '';
  fullUrl = '';
  recipientUserId = '';
  isRead = false;

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getPostPayload: () => {
        const payload = {
          title: this.title,
          content: this.content,
          url: this.url,
          fullUrl: this.fullUrl,
          recipientUserId: this.recipientUserId,
          isRead: this.isRead,
          createdAt: new Date(),
          updatedAt: new Date(),
        };

        return payload;
      },
      getPatchPayload: () => {
        const payload = {
          isRead: this.isRead,
          updatedAt: new Date(),
        };

        return payload;
      },
    });
  };
}
