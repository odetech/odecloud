import { requests } from './requests';
import { Notification } from './model';
import type NotificationTypes from './types';

const notifications = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Notification,
    payload: { ...new Notification({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  NotificationTypes,
};

export default notifications;
