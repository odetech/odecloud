import { BasicModel } from '../basicModel';
import FeedTypes from './types';

export class Feed extends BasicModel implements FeedTypes {
  _id = '';
  isDraft = true;
  title = '';
  content = '';
  externalUrl = ''; // If the article is actually a link to an external articles written on Medium, etc
  rawContent = '';
  tags = []; // string of tag._id
  files = []; // list of cloudinary object or file._id
  likes = []; // string of user._id
  views = []; // string of user._id
  followers = []; // string of user._id
  publishedAt = null;
  publishedNickname = '';
  isByStaff = false;
  mentions = [];

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getPostPayload: () => {
        const payload = {
          title: this.title,
          content: this.content,
          isDraft: this.isDraft,
          rawContent: this.rawContent,
          isByStaff: this.isByStaff,
          externalUrl: this.externalUrl,
          publishedNickname: this.publishedNickname,
          tags: this.tags,
          likes: this.likes,
          files: this.files,
          views: this.views,
          followers: this.followers,
          publishedAt: this.publishedAt,
          createdBy: this.createdBy,
          updatedBy: this.updatedBy,
          createdAt: new Date(),
          updatedAt: new Date(),
          mentions: this.mentions,
        };

        return payload;
      },
    });
  };
}
