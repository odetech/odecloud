import { expect } from 'chai';
import { Feed } from './model';

describe('Feed\'s Model', () => {
  const MockFeed = () => new Feed({
    _id: '123456',
    isDraft: false,
    title: 'Hello World',
    content: 'Lorem ipsum dolor sit amet',
    externalUrl: 'String',
    rawContent: 'String',
    tags: [],
    views: [],
    files: [],
    likes: [],
    followers: [],
    mentions: [],
    publishedAt: null,
    publishedNickname: 'String',
    isByStaff: false,
  });

  it('checks basic', () => {
    const feed = MockFeed();

    expect(feed._id).to.equal('123456');
    expect(feed.isDraft).to.equal(false);
    expect(feed.title).to.equal('Hello World');
    expect(feed.content).to.equal('Lorem ipsum dolor sit amet');
    expect(feed.externalUrl).to.equal('String');
    expect(feed.rawContent).to.equal('String');
    expect(0).to.equal(feed.followers.length);
    expect(0).to.equal(feed.tags.length);
    expect(0).to.equal(feed.files.length);
    expect(0).to.equal(feed.likes.length);
    expect(0).to.equal(feed.views.length);
    expect(0).to.equal(feed.mentions.length);
    expect(feed.publishedAt).to.equal(null);
    expect(feed.publishedNickname).to.equal('String');
    expect(feed.isByStaff).to.equal(false);
  });

  describe('payload()', () => {
    const feed = MockFeed();

    it('checks getPostPayload()', () => {
      const payload = feed.payload().getPostPayload();

      expect(payload.title).to.equal(feed.title);
      expect(payload.content).to.equal(feed.content);
      expect(payload.tags).to.equal(feed.tags);
      expect(payload.views).to.equal(feed.views);
      expect(payload.likes.length).to.equal(feed.likes.length);
      expect(payload.followers).to.equal(feed.followers);
      expect(payload.files).to.equal(feed.files);
      expect(payload.mentions.length).to.equal(feed.mentions.length);
      expect(payload.isDraft).to.equal(feed.isDraft);
      expect(payload.publishedAt).to.equal(feed.publishedAt);
      expect(new Date(payload.createdAt).getTime() > new Date(feed.createdAt).getTime()).to.equal(true);
      expect(new Date(payload.updatedAt).getTime() > new Date(feed.updatedAt).getTime()).to.equal(true);
      expect(payload.updatedBy).to.equal(feed.updatedBy);
      expect(payload.createdBy).to.equal(feed.createdBy);
    });

  });
});
