import { requests } from './requests';
import { Feed } from './model';
import type FeedTypes from './types';

const feed = (getRequestOptions: any, getEndpoint: any) => ({
  model: Feed,
  payload: { ...new Feed({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  FeedTypes,
};

export default feed;
