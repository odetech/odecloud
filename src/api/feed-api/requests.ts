import {
  get,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getFeed: async (searchParams?: {
    data?: string[],
    loadBy?: string[],
    tags?: string[],
    exclude?: string[],
    since?: string,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.tags) {
      delete searchParamsCopy.tags;
    }
    if (searchParamsCopy.exclude) {
      delete searchParamsCopy.exclude;
    }
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    if (searchParamsCopy.loadBy) {
      delete searchParamsCopy.loadBy;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.tags) {
      searchParams.tags.forEach((item) => {
        searchParamsString.append('tags', item);
      });
    }
    if (searchParams?.exclude) {
      searchParams.exclude.forEach((item) => {
        searchParamsString.append('exclude', item);
      });
    }
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }
    if (searchParams?.loadBy) {
      searchParams.loadBy.forEach((item) => {
        searchParamsString.append('loadBy', item);
      });
    }

    let url = getEndpoint('feed');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const articles = await get(
        getRequestOptions,
        url,
      );
      return articles;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getPrivateChatsFeed: async (searchParams?: {
    data?: string[],
    loadBy?: string[],
    tags?: string[],
    exclude?: string[],
    since?: string,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.tags) {
      delete searchParamsCopy.tags;
    }
    if (searchParamsCopy.exclude) {
      delete searchParamsCopy.exclude;
    }
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    if (searchParamsCopy.loadBy) {
      delete searchParamsCopy.loadBy;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.tags) {
      searchParams.tags.forEach((item) => {
        searchParamsString.append('tags', item);
      });
    }
    if (searchParams?.exclude) {
      searchParams.exclude.forEach((item) => {
        searchParamsString.append('exclude', item);
      });
    }
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }
    if (searchParams?.loadBy) {
      searchParams.loadBy.forEach((item) => {
        searchParamsString.append('loadBy', item);
      });
    }

    let url = getEndpoint('feed/private-chats');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const articles = await get(
        getRequestOptions,
        url,
      );
      return articles;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  search: async (searchParams: {
    search: string,
    data?: string[],
    tags?: string[],
    exclude?: string[],
    since?: string,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.tags) {
      delete searchParamsCopy.tags;
    }
    if (searchParamsCopy.exclude) {
      delete searchParamsCopy.exclude;
    }
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.tags) {
      searchParams.tags.forEach((item) => {
        searchParamsString.append('tags', item);
      });
    }
    if (searchParams?.exclude) {
      searchParams.exclude.forEach((item) => {
        searchParamsString.append('exclude', item);
      });
    }
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint('feed/search');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const articles = await get(
        getRequestOptions,
        url,
      );
      return articles;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
