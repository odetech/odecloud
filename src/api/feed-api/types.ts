type FeedTypes = {
  _id: string,
  isDraft: boolean,
  title: string,
  content: string,
  externalUrl: string,
  rawContent: string,
  tags: string[] | [],
  views: string[] | [],
  likes: string[] | [],
  followers: string[] | [],
  publishedAt: any | null,
  publishedNickname: string,
  isByStaff: boolean,
  mentions: string[],
};

export default FeedTypes;
