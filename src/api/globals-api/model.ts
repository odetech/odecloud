import GlobalsTypes from './types';

export class Globals implements GlobalsTypes {
  questionnaireSequence = [];

  constructor(data: any) {
    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPatchPayload: (patchPayload: any) => {
      const payload: {
        questionnaireSequence?: string[],
      } = {};

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
  });
}
