import { requests } from './requests';
import { Globals } from './model';
import type GlobalsTypes from './types';

const globals = (getRequestOptions: any, getEndpoint: any) => ({
  model: Globals,
  payload: { ...new Globals({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  GlobalsTypes,
};

export default globals;
