import {
  get, patch, post, deleteRequest,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getGlobals: async () => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('globals'),
        {},
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint('globals'),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
