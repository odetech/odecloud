import AppStoreTypes from './types';

export class AppStore implements AppStoreTypes {
  _id = '';
  apps = ['odesocial', 'odetask'];
  secret = '1234';

  constructor(data: any) {
    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getPostAuthRequestPayload: () => {
        const payload = null;

        return payload;
      },
      getPostAuthLoginPayload: () => {
        const payload = {
          secret: this.secret,
          when: new Date(),
        };

        return payload;
      },
      getPatchPayload: () => {
        const payload = {
          apps: this.apps,
        };

        return payload;
      },
    });
  };
}
