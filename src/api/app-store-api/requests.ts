import { patch, post } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  patch: async (appStoreId: string, payload: any) => {
    try {
      const resultId = await patch(
        getRequestOptions,
        getEndpoint(`app-stores/${appStoreId}`),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
  authRequest: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('app-stores/auth/request'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
  authLogin: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('app-stores/auth/login'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
  createAppStore: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('app-stores'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
});
