import { expect } from 'chai';
import { AppStore } from './model';

describe('App Store\'s Model', () => {
  const MockAppStore = () => new AppStore({
    _id: '123456',
    apps: ['odesocial', 'odetask'],
  });

  const MockAuth = () => new AppStore({
    secret: '1234',
    when: new Date(),
  });

  it('checks basic', () => {
    const appStore = MockAppStore();

    expect(appStore._id).to.equal('123456');
    expect(appStore.apps[0]).to.equal('odesocial');
    expect(appStore.apps[1]).to.equal('odetask');
  });

  describe('payload()', () => {
    let appStore = MockAppStore();

    it('checks getPatchPayload()', () => {
      const payload = appStore.payload().getPatchPayload();
      expect(payload.apps[0]).to.equal(appStore.apps[0]);
      expect(payload.apps[1]).to.equal(appStore.apps[1]);
    });

    appStore = MockAuth();

    it('checks getPostAuthRequestPayload()', () => {
      const payload = appStore.payload().getPostAuthRequestPayload();
      expect(payload).to.equal(null);
    });

    it('checks getPostAuthLoginPayload()', () => {
      const payload = appStore.payload().getPostAuthLoginPayload();
      expect(payload.secret).to.equal(appStore.secret);
    });
  });
});
