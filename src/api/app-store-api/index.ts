import { requests } from './requests';
import { AppStore } from './model';
import type AppStoreTypes from './types';

const appStores = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: AppStore,
    payload: { ...new AppStore({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  AppStoreTypes,
};

export default appStores;
