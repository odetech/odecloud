type AppStoreTypes = {
  _id: string,
  apps: string[],
  tokens?: string[],
};

export default AppStoreTypes;
