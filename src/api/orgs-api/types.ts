type OrgTypes = {
  _id: string,
  name: string,
  tagline: string,
  website: string,
  isActive: boolean,
  geoInfo: {
    address: string,
    city: string,
    state: string,
    zip: string,
    country: string,
  },
};

export default OrgTypes;
