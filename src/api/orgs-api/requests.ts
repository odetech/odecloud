import {
  get, patch, post, deleteRequest,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getOrgs: async (searchParams?: {
    isActive?: boolean,
    createdBy?: string,
    updatedBy?: string,
    pageNumber?: number,
    limit?: number,
    isStaff?: number,
    isDev?: number,
    projectAssignedToUserId?: string,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('orgs'),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getOrg: async (orgId: string, searchParams?: {
    isActive?: boolean,
    createdBy?: string,
    updatedBy?: string,
    pageNumber?: number,
    limit?: number,
    isStaff?: number,
    isDev?: number,
    projectAssignedToUserId?: string,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`orgs/${orgId}`),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint('orgs'),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (orgId: string) => {
    try {
      const request = await deleteRequest(
        getRequestOptions,
        getEndpoint(`orgs/${orgId}`),
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (orgId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`orgs/${orgId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
