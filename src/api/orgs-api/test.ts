import { expect } from 'chai';
import { Org } from './model';

describe('Org\'s Model', () => {
  const MockOrg = () => new Org({
    _id: '123456',
    name: 'qwe',
    tagline: 'asd',
    website: 'zxc',
    isActive: true,
    geoInfo: {
      address: 'wer',
      city: 'sdf',
      state: 'xcv',
      zip: 'rty',
      country: 'fgh',
    },
  });

  it('checks basic', () => {
    const org = MockOrg();

    expect(org._id).to.equal('123456');
    expect(org.name).to.equal('qwe');
    expect(org.tagline).to.equal('asd');
    expect(org.website).to.equal('zxc');
    expect(org.isActive).to.equal(true);
    expect(org.geoInfo.address).to.equal('wer');
    expect(org.geoInfo.city).to.equal('sdf');
    expect(org.geoInfo.state).to.equal('xcv');
    expect(org.geoInfo.zip).to.equal('rty');
    expect(org.geoInfo.country).to.equal('fgh');
  });

  describe('payload()', () => {
    const org = MockOrg();

    it('checks getPostPayload()', () => {
      const payload = org.payload().getPostPayload({
        name: 'dfg',
      });

      expect(payload.name).to.equal('dfg');
    });

    it('checks getPatchPayload()', () => {
      const payload = org.payload().getPatchPayload({
        tagline: 'sdf',
      });

      expect(payload.tagline).to.equal('sdf');
    });

  });
});
