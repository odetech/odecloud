import { requests } from './requests';
import { Org } from './model';
import type OrgTypes from './types';

const orgs = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Org,
    payload: { ...new Org({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  OrgTypes,
};

export default orgs;
