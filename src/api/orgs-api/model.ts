import { BasicModel } from '../basicModel';
import OrgTypes from './types';

export class Org extends BasicModel implements OrgTypes {
  _id = '';
  name = '';
  tagline = '';
  website = '';
  isActive = true;
  geoInfo = {
    address: '',
    city: '',
    state: '',
    zip: '',
    country: '',
  };

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload: any) => {
      const payload: {
        name: string,
        tagline?: string,
        website?: string,
        isActive?: boolean,
        geoInfo?: {
          address?: string,
          city?: string,
          state?: string,
          zip?: string,
          country?: string,
        },
      } = {
        name: '',
      };

      const keys = Object.keys(postPayload);
      keys.forEach((key: string) => {
        if (postPayload[key] !== undefined) {
          payload[key] = postPayload[key];
        }
      });

      return payload;
    },
    getPatchPayload: (patchPayload: any) => {
      const payload: {
        name?: string,
        tagline?: string,
        website?: string,
        isActive?: boolean,
        geoInfo?: {
          address?: string,
          city?: string,
          state?: string,
          zip?: string,
          country?: string,
        },
      } = {};

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
  });
}
