import { requests } from './requests';
import { Question } from './model';
import type QuestionTypes from './types';

const questions = (getRequestOptions: any, getEndpoint: any) => ({
  model: Question,
  payload: { ...new Question({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  QuestionTypes,
};

export default questions;
