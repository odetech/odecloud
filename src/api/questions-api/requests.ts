import { deleteRequest, get, post } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getQuestions: async (searchParams?: {
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const questions = await get(
        getRequestOptions,
        getEndpoint('questions'),
        searchParams,
      );
      return questions;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getQuestion: async (questionId: string, searchParams?: {
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const question = await get(
        getRequestOptions,
        getEndpoint(`questions/${questionId}`),
        searchParams,
      );
      return question;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('questions/options'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (questionId: string) => {
    try {
      const resultId = await deleteRequest(
        getRequestOptions,
        getEndpoint(`questions/${questionId}`),
      );
      return resultId;
    } catch(e) {
      // handle error
      throw e;
    }
  },
});
