import { expect } from 'chai';
import { Question } from './model';

describe('Question\'s Model', () => {
  const MockQuestion = () => new Question({
    _id: 'asd',
    title: 'sdf',
    type: 'dfg',
    multiple: true,
    options: [{
      label: 'sdf',
      value: 1,
    }],
  });

  it('checks basic', () => {
    const question = MockQuestion();

    expect(question._id).to.equal('asd');
    expect(question.title).to.equal('sdf');
    expect(question.type).to.equal('dfg');
    expect(question.multiple).to.equal(true);
    expect(question.options[0].label).to.equal('sdf');
  });

  describe('payload()', () => {
    const question = MockQuestion();

    it('checks getPostPayload()', () => {
      const payload = question.payload().getPostPayload({
        title: 'sdf',
        type: 'dfg',
        multiple: true,
        options: [{
          label: 'sdf',
          value: 1,
        }],
      });

      expect(payload.title).to.equal('sdf');
      expect(payload.type).to.equal('dfg');
      expect(payload.multiple).to.equal(true);
      expect(payload.options[0].value).to.equal(1);
    });
  });
});
