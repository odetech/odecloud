import QuestionTypes from './types';
import { BasicModel } from '../basicModel';

export class Question extends BasicModel implements QuestionTypes {
  _id = '';
  title = '';
  type = '';
  multiple = false;
  options = [];

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload?: QuestionTypes) => {
      const payload = {
        title: postPayload?.title || this.title,
        type: postPayload?.type || this.type,
        multiple: postPayload?.multiple || this.multiple,
        options: postPayload?.options || this.options,
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      return payload;
    },
  });
}
