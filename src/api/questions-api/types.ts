type QuestionTypes = {
  _id?: string,
  title: string,
  type?: string;
  multiple: boolean,
  options: {
    label: string,
    value: number,
  }[],
  createdBy?: string;
  updatedBy?: string,
  createdAt?: Date | null,
  updatedAt?: Date | null,
};

export default QuestionTypes;
