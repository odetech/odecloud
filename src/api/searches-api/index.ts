import { requests } from './requests';

const searches = (getRequestOptions: any, getEndpoint: any) => ({
  ...requests(getRequestOptions, getEndpoint),
});

export default searches;
