import {
  get,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getSearchQueries: async (searchParams?: {
    searchStr: string,
    projectId?: string,
    query: string[],
    isActive?: boolean,
    createdBy?: string,
    pageNumber?: number,
    limit?: number,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.query) {
      delete searchParamsCopy.query;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.query) {
      searchParams.query.forEach((item) => {
        searchParamsString.append('query', item);
      });
    }

    let url = getEndpoint('searches');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
