import { requests } from './requests';
import { Expertise } from './model';
import type ExpertiseTypes from './types';

const expertises = (getRequestOptions: any, getEndpoint: any) => ({
  model: Expertise,
  payload: { ...new Expertise({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  ExpertiseTypes,
};

export default expertises;
