import { expect } from 'chai';
import { Expertise } from './model';

describe('Expertise\'s Model', () => {
  const MockExpertise = () => new Expertise({
    _id: 'wegw23egdfg5we4g',
    updatedAt: '2018-12-25T21:26:20.036Z',
    expertise: {
      EXNID: [
        {
          value: 'someSkill1',
          rating: 5,
        },
        {
          value: 'someSkill2',
          rating: 4,
        },
        {
          value: 'someSkill3',
          rating: 9,
        },
      ],
      EXFAC: [],
      EXSASPRO: [],
      EXENT: [],
      OTHER: []
    },
    userId: 'DtpAnwgoTnt57g9ht',
    updatedBy: 'DtpAnwgoTnt57g9ht',
  });

  it('checks basic', () => {
    const expertise = MockExpertise();

    expect(expertise._id).to.equal('wegw23egdfg5we4g');
    expect(expertise.expertise.EXNID[0].value).to.equal('someSkill1');
    expect(expertise.expertise.EXNID[0].rating).to.equal(5);
    expect(expertise.userId).to.equal('DtpAnwgoTnt57g9ht');
    expect(expertise.updatedBy).to.equal('DtpAnwgoTnt57g9ht');
  });

  describe('payload()', () => {
    const expertise = MockExpertise();

    it('checks getInvitePayload()', () => {
      const payload = expertise.payload().getPatchPayload({
        expertise: {
          EXNID: [],
          EXFAC: [],
          EXSASPRO: [
            {
              value: 'someSkill1',
              rating: 5,
            },
            {
              value: 'someSkill2',
              rating: 4,
            },
            {
              value: 'someSkill3',
              rating: 9,
            },
          ],
          EXENT: [],
          OTHER: []
        }
      });
      expect(payload.expertise.EXSASPRO[2].rating).to.equal(9);
    });

    it('checks getJoinInvitationsPayload()', () => {
      const payload = expertise.payload().getPostPayload({
        updatedBy: 'DtpAnwgoTnt57g9ht',
        userId: 'DtpAnwgoTnt57g9ht',
      });
      expect(payload.updatedBy).to.equal('DtpAnwgoTnt57g9ht');
      expect(payload.userId).to.equal('DtpAnwgoTnt57g9ht');
      expect(payload.expertise.EXSASPRO.length).to.equal(0);
    });
  });
});
