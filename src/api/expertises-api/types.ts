type ExpertiseTypes = {
  expertise: {
    "EXNID": any[],
    "EXFAC": any[],
    "EXSASPRO": any[],
    "EXENT": any[],
    "OTHER": any[]
  } | null,
  updatedBy: string | null,
  userId: string | null,
  updatedAt: null | Date,
  "_id": string,
};

export default ExpertiseTypes;
