import ExpertiseTypes from './types';

export class Expertise implements ExpertiseTypes {
  _id = '';
  updatedAt = null;
  expertise = {
    EXNID: [],
    EXFAC: [],
    EXSASPRO: [],
    EXENT: [],
    OTHER: []
  };
  userId = '';
  updatedBy = '';

  constructor(data: any) {
    if (!data) {
      return;
    }

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPatchPayload: (payloadData: {
      expertise: {
        EXNID: any[],
        EXFAC: any[],
        EXSASPRO: any[],
        EXENT: any[],
        OTHER: any[],
      },
    }) => {
      const payload = {
        expertise: payloadData.expertise || this.expertise,
      };

      return payload;
    },

    getPostPayload: (payloadData: {
      updatedBy: string,
      userId: string,
      expertise?: {
        EXNID: any[],
        EXFAC: any[],
        EXSASPRO: any[],
        EXENT: any[],
        OTHER: any[],
      },
    }) => {
      const payload = {
        updatedBy: payloadData.updatedBy,
        userId: payloadData.userId,
        expertise: payloadData.expertise || this.expertise,
      };

      return payload;
    },
  });
}
