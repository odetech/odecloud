import { post, patch } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  patch: async (expertiseId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`expertises/${expertiseId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: {
    emails: string[],
    subject: string,
    message: string,
  }) => {
    try {
      const requestData = await post(
        getRequestOptions,
        getEndpoint('expertises'),
        payload,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
