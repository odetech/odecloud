import { BasicModel } from '../basicModel';
import AvatarTypes from './types';

export class Avatar extends BasicModel implements AvatarTypes {
  _id = '';

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }
}
