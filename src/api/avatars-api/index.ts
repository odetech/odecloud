import { requests } from './requests';
import { Avatar } from './model';
import type AvatarTypes from './types';

const avatars = (getRequestOptions: any, getEndpoint: any) => ({
  model: Avatar,
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  AvatarTypes,
};

export default avatars;
