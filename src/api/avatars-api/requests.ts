import {
  get,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getAvatars: async (searchParams?: {
    userId?: string[],
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.userId) {
      delete searchParamsCopy.userId;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.userId) {
      searchParams.userId.forEach((item) => {
        searchParamsString.append('userId', item);
      });
    }

    let url = getEndpoint('avatars');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getAvatar: async (avatarId: string, searchParams?: {}) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`avatars/${avatarId}`),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
