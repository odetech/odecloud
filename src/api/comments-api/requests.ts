import {
  get, patch, post, deleteRequest,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getComments: async (searchParams?: {
    associatedId?: number,
    data?: string[],
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint('comments');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getComment: async (commentId: string, searchParams?: {
    associatedId?: number,
    data?: string[],
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint(`comments/${commentId}`);
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (
    payload: any,
    additionalParams?: {
      app: string | 'odesocial' | 'odetask',
      parent: string | 'article' | 'task' | 'comment',
      isChat: boolean,
    },
  ) => {
    let url = getEndpoint('comments');

    if (additionalParams) {
      const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
      const searchParamsString = new URLSearchParams(searchParamsCopy);

      url = url.concat(`?${searchParamsString.toString()}`);
    }

    try {
      const resultId = await post(
        getRequestOptions,
        url,
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  postFiles: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('comments/files'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (commentId: string) => {
    try {
      const resultId = await deleteRequest(
        getRequestOptions,
        getEndpoint(`comments/${commentId}`),
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (commentId: string, payload: any) => {
    try {
      const resultId = await patch(
        getRequestOptions,
        getEndpoint(`comments/${commentId}`),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patchFiles: async (articleId: string, payload: any) => {
    try {
      const resultId = await patch(
        getRequestOptions,
        getEndpoint(`comments/${articleId}/files`),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  search: async (searchStr: string) => {
    if (!searchStr) {
      return;
    }

    const payload = {
      searchStr,
    };

    try {
      const resultId = await get(
        getRequestOptions,
        getEndpoint('comments/search'),
        payload,
      );

      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  likes: {
    // For both PATCH & DELETE API will assume the current authenticated user as the user who will be liking
    // a comment. Therefore, a user id is not required but just the article id.
    patch: async (commentId: string) => {
      try {
        const resultId = await patch(
          getRequestOptions,
          getEndpoint(`comments/likes/${commentId}`),
          {},
        );
        return resultId;
      } catch (e) {
        throw e; // handle error
      }
    },
    delete: async (commentId: string) => {
      try {
        const resultId = await deleteRequest(
          getRequestOptions,
          getEndpoint(`comments/likes/${commentId}`),
        );
        return resultId;
      } catch (e) {
        // handle error
        throw e;
      }
    },
  },
});
