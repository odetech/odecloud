import { requests } from './requests';
import { Comment } from './model';
import type CommentTypes from './types';

const comments = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Comment,
    payload: { ...new Comment({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  CommentTypes,
};

export default comments;
