import { User } from '../users-api/model';

type CommentTypes = {
  _id: string,
  value: string,
  rawValue: string,
  associatedId: string,
  userInfo: User | null,
  thirdParties: any | null,
  isPublic: boolean,
  mentions: string[],
  files: any[],
  likes: string[],
  data: any | null,
};

export default CommentTypes;
