import { BasicModel } from '../basicModel';
import CommentTypes from './types';

export class Comment extends BasicModel implements CommentTypes {
  _id = '';
  value = '';
  rawValue = '';
  associatedId = '';
  userInfo = null;
  thirdParties = {};
  isPublic = true;
  files = []; // Cloudinary Object
  likes = []; // String of `userId`
  mentions = [];
  data = []; // array of ['createdBy', 'associatedId', 'files']
  target = 'articles/comments'; // enum [ orgs/comments, projects/comments, articles/comments, task/comments ]

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getPostPayload: () => {
        const payload = {
          value: this.value,
          rawValue: this.rawValue,
          associatedId: this.associatedId,
          userInfo: this.userInfo,
          thirdParties: this.thirdParties,
          isPublic: this.isPublic,
          mentions: this.mentions,
          files: this.files,
          likes: this.likes,
          data: this.data,
          createdBy: this.createdBy,
          updatedBy: this.updatedBy,
          createdAt: new Date(),
          updatedAt: new Date(),
        };

        return payload;
      },

      getFilesPostPayload: () => {
        const payload = {
          comment: {
            value: this.value,
            rawValue: this.rawValue,
            associatedId: this.associatedId,
            userInfo: this.userInfo,
            thirdParties: this.thirdParties,
            isPublic: this.isPublic,
            mentions: this.mentions,
            files: this.files,
            likes: this.likes,
            data: this.data,
            createdBy: this.createdBy,
            updatedBy: this.updatedBy,
            createdAt: new Date(),
            updatedAt: new Date(),
          },
          target: this.target,
          files: this.files,
        };

        return payload;
      },

      getPatchPayload: () => {
        const payload = {
          value: this.value,
          rawValue: this.rawValue,
        };

        return payload;
      },

      getFilesPatchPayload: () => {
        const payload = {
          comment: {
            value: this.value,
            rawValue: this.rawValue,
          },
          insertions: this.files,
          deletions: this.files,
        };

        return payload;
      },
    });
  };
}
