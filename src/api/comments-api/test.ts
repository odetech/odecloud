import { expect } from 'chai';
import { Comment } from './model';

describe('Comment\'s Model', () => {
  const MockComment = () => new Comment( {
    createdBy: 'SG8aYLxtAK7cbpakm',
    updatedBy: 'SG8aYLxtAK7cbpakm',
    createdAt: '2021-02-17T02:08:06.709Z',
    updatedAt: '2021-02-17T02:08:06.709Z',
    _id: '2owWTsJA4FRZdzRG4',
    value: '<p>Agree will move this to ongoing.</p>\n',
    rawValue: '',
    associatedId: 'oBemkpXXCGmBtBs2J',
    userInfo: null,
    thirdParties: null,
    isPublic: true,
    files: [],
    likes: [],
    mentions: [],
    target: 'articles/comments',
  });

  it('checks basic', () => {
    const comment = MockComment();

    expect(comment._id).to.equal('2owWTsJA4FRZdzRG4');
    expect(comment.value).to.equal('<p>Agree will move this to ongoing.</p>\n');
    // TODO: When instantiating new Comment, 'value' should then be also be duplicated onto
    // 'rawValue' as comment.rawValue=extractHtmlContent(comment.value)
    expect(comment.rawValue).to.equal('');
    expect(comment.associatedId).to.equal('oBemkpXXCGmBtBs2J');
    expect(comment.userInfo).to.equal(null);
    expect(comment.thirdParties).to.equal(null);
    expect(comment.isPublic).to.equal(true);
    expect(comment.files.length).to.equal(0);
    expect(comment.likes.length).to.equal(0);
    expect(comment.mentions.length).to.equal(0);
  });

  describe('payload()', () => {
    const comment = MockComment();

    it('checks getPostPayload()', () => {
      const payload = comment.payload().getPostPayload();

      expect(payload.value).to.equal(comment.value);
      expect(payload.rawValue).to.equal(comment.rawValue);
      expect(payload.associatedId).to.equal(comment.associatedId);
      expect(payload.thirdParties).to.equal(comment.thirdParties);
      expect(payload.createdBy).to.equal(comment.createdBy);
      expect(payload.likes).to.equal(comment.likes);
      expect(payload.isPublic).to.equal(comment.isPublic);
      expect(payload.files).to.equal(comment.files);
      expect(payload.mentions).to.equal(comment.mentions);

      expect(new Date(payload.createdAt).getTime() > new Date(comment.createdAt).getTime()).to.equal(true);
      expect(new Date(payload.updatedAt).getTime() > new Date(comment.updatedAt).getTime()).to.equal(true);
      expect(payload.updatedBy).to.equal(comment.updatedBy);
      expect(payload.createdBy).to.equal(comment.createdBy);
    });

    it('checks getFilesPostPayload()', () => {
      const payload = comment.payload().getFilesPostPayload();

      expect(payload.comment.value).to.equal(comment.value);
      expect(payload.comment.rawValue).to.equal(comment.rawValue);
      expect(payload.comment.associatedId).to.equal(comment.associatedId);
      expect(payload.comment.thirdParties).to.equal(comment.thirdParties);
      expect(payload.comment.createdBy).to.equal(comment.createdBy);
      expect(payload.comment.likes).to.equal(comment.likes);
      expect(payload.comment.isPublic).to.equal(comment.isPublic);
      expect(payload.files).to.equal(comment.files);
      expect(payload.comment.mentions).to.equal(comment.mentions);
      expect(payload.target).to.equal(comment.target);

      expect(new Date(payload.comment.createdAt).getTime() > new Date(comment.createdAt).getTime()).to.equal(true);
      expect(new Date(payload.comment.updatedAt).getTime() > new Date(comment.updatedAt).getTime()).to.equal(true);
      expect(payload.comment.updatedBy).to.equal(comment.updatedBy);
      expect(payload.comment.createdBy).to.equal(comment.createdBy);
    });

    it('checks getPatchPayload()', () => {
      const payload = comment.payload().getPatchPayload();

      expect(payload.value).to.equal(comment.value);
      expect(payload.rawValue).to.equal(comment.rawValue);
    });

    it('checks getFilesPatchPayload()', () => {
      const payload = comment.payload().getFilesPatchPayload();

      expect(payload.comment.value).to.equal(comment.value);
      expect(payload.comment.rawValue).to.equal(comment.rawValue);
    });
  });
});
