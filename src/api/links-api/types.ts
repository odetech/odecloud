type LinkTypes = {
  _id: string,
  link: string,
  shortLink: string,
  visitorId: string[],
  sharedBy: string[],
  createdAt: Date,
  updatedAt: Date,
  extra: {
    message: {
      _id: string,
      associatedId: string | null,
    }
  }
};

export type PatchPayload = {
  sharedBy?: string[],
  visitorId?: string[],
  updatedAt?: string,
}

export default LinkTypes;
