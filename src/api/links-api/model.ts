import { BasicModel } from '../basicModel';
import LinkTypes from './types';

export class Link extends BasicModel implements LinkTypes {
  _id = '';
  link = '';
  shortLink = '';
  visitorId = []; // string of user._id
  sharedBy = []; // string of user._id
  extra = {
    message: {
      _id: '',
      associatedId: null,
    }
  };

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getPostPayload: () => {
        const payload = {
          link: this.link,
          shortLink: this.shortLink,
          visitorId: this.visitorId,
          sharedBy: this.sharedBy,
          createdAt: new Date(),
          updatedAt: new Date(),
          extra: this.extra,
        };

        return payload;
      },

      getPatchPayload: () => {
        const payload = {
          visitorId: this.visitorId,
          sharedBy: this.sharedBy,
          updatedAt: new Date(),
        };

        return payload;
      },
    });
  };
}
