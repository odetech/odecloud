import {
  post, get, patch,
} from '../../base-requests';
import { PatchPayload } from "./types";

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getLinks: async (
    searchParams?: {
      shortLink?: string,
      visitorId?: string,
      pageNumber?: number,
      limit?: number,
      createdBy?: string,
      updatedBy?: string,
      isStaff?: number,
      isDev?: number,
    },
  ) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('links'),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getLink: async (
    linkId: string,
    searchParams?: {
      shortLink?: string,
      visitorId?: string,
      pageNumber?: number,
      limit?: number,
      createdBy?: string,
      updatedBy?: string,
      isStaff?: number,
      isDev?: number,
    },
  ) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`links/${linkId}`),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (
    payload: {
      link: string,
      sharedBy: string[],
      extra: {
        message: {
          _id: string,
        }
      }
    },
    additionalParams: {
      origin: string | 'odesocial' | 'odetask' | 'chat',
    }
  ) => {
    try {
      let url = getEndpoint('links');
      if (additionalParams) {
        const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
        const searchParamsString = new URLSearchParams(searchParamsCopy);

        url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
      }

      const request = await post(
        getRequestOptions,
        url,
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (linkId: string, payload: PatchPayload) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`links/${linkId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
