import { expect } from 'chai';
import { Link } from './model';

describe('Link\'s Model', () => {
  const MockLink = () => new Link({
    _id: 'ab406e80-60fe-4485-9bbf-469e8dc5b78f',
    createdAt: new Date('2022-09-23T16:14:04.546000'),
    link: 'endpoint/route?activeTask=task--Sy4HuDecQepMvrrMk--Sy4HuDecQepMvrrMk--8f084d',
    sharedBy: ['Wj2PKhcSacD3YiHaP'],
    shortLink: 'link/r/ab406e80-60fe-4485-9bbf-469e8dc5b78f?visitorId=',
    visitorId: ['Wj2PKhcSacD3YiHaP'],
    updatedAt: new Date('2022-09-23T16:14:04.546000'),
    extra: {
      message: {
        _id: 'task--Sy4HuDecQepMvrrMk--Sy4HuDecQepMvrrMk--8f084d',
        associatedId: null,
      }
    }
  });

  it('checks basic', () => {
    const link = MockLink();

    expect(link._id).to.equal('ab406e80-60fe-4485-9bbf-469e8dc5b78f');
    expect(link.link).to.equal('endpoint/route?activeTask=task--Sy4HuDecQepMvrrMk--Sy4HuDecQepMvrrMk--8f084d');
    expect(link.shortLink).to.equal('link/r/ab406e80-60fe-4485-9bbf-469e8dc5b78f?visitorId=');
  });

  describe('payload()', () => {
    const link = MockLink();

    it('checks getPostPayload()', () => {
      const payload = link.payload().getPostPayload();

      expect(payload.link).to.equal(link.link);
      expect(payload.sharedBy).to.equal(link.sharedBy);
      expect(payload.extra).to.equal(link.extra);
    });

    it('checks getPatchPayload()', () => {
      const payload = link.payload().getPatchPayload();

      expect(payload.sharedBy[0]).to.equal(link.sharedBy[0]);
      expect(payload.visitorId[0]).to.equal(link.visitorId[0]);
    });
  });
});
