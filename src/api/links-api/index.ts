import { requests } from './requests';
import { Link } from './model';
import type LinkTypes from './types';

const links = (getRequestOptions: any, getEndpoint: any) => ({
  model: Link,
  payload: { ...new Link({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  LinkTypes,
};

export default links;
