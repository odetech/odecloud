import { BasicModel } from '../basicModel';
import HashtagTypes from './types';

export class Hashtag extends BasicModel implements HashtagTypes {
  _id = '';
  createdBy = '';
  value = '';

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getPostPayload: () => {
        const payload = {
          value: this.value,
          createdBy: this.createdBy,
        };
        return payload;
      },
    });
  };
}
