import { requests } from './requests';
import { Hashtag } from './model';
import type HashtagTypes from './types';

const hashtags = (getRequestOptions: any, getEndpoint: any) => ({
  model: Hashtag,
  payload: { ...new Hashtag({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  HashtagTypes,
};

export default hashtags;
