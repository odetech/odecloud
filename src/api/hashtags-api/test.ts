import { expect } from 'chai';
import { Hashtag } from './model';

describe('Hashtag\'s Model', () => {
  const MockHashtag = () => new Hashtag({
    _id: 'ab406e80-60fe-4485-9bbf-469e8dc5b78f',
    createdBy: 'Wj2PKhcSacD3YiHaP',
    value: "testHash",
  });

  it('checks basic', () => {
    const hashtag = MockHashtag();

    expect(hashtag._id).to.equal('ab406e80-60fe-4485-9bbf-469e8dc5b78f');
    expect(hashtag.createdBy).to.equal('Wj2PKhcSacD3YiHaP');
    expect(hashtag.value).to.equal('testHash');
  });

  describe('payload()', () => {
    const hashtag = MockHashtag();

    it('checks getPostPayload()', () => {
      const payload = hashtag.payload().getPostPayload();

      expect(payload.value).to.equal(hashtag.value);
      expect(payload.createdBy).to.equal(hashtag.createdBy);
    });
  });
});
