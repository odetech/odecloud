import {
  post, get,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getHashtags: async (
    searchParams?: {
      searchStr: string,
    },
  ) => {
    const copyParams = { ...searchParams };
    if("searchStr" in copyParams && !copyParams.searchStr) {
      delete copyParams.searchStr;
    }
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('hashtags'),
        copyParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (
    payload: {
      value: string,
      createdBy: string,
    },
  ) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint('hashtags'),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
