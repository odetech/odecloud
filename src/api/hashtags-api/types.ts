type HashtagTypes = {
  _id: string,
  value: string,
};

export default HashtagTypes;
