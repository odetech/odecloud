import { expect } from 'chai';
import { File } from './model';

describe('Files\'s Model', () => {
  const MockFile = () => new File({
    image: '123',
    target: 'Hello World',
    createdBy: 'Lorem ipsum dolor sit amet',
    associatedId: 'String',
  });

  it('checks basic', () => {
    const file = MockFile();

    expect(file.image).to.equal('123');
    expect(file.target).to.equal('Hello World');
    expect(file.createdBy).to.equal('Lorem ipsum dolor sit amet');
    expect(file.associatedId).to.equal('String');
  });

  // describe('payload()', () => {
  //   const file = MockFile();
  //
  //   it('checks getPostPayload()', () => {
  //     const payload = file.payload().getPostPayload();
  //
  //     expect(payload.get('image')).to.equal('123');
  //     expect(payload.get('target')).to.equal('Hello World');
  //     expect(payload.get('createdBy')).to.equal('Lorem ipsum dolor sit amet');
  //     expect(payload.get('associatedId')).to.equal('String');
  //   });
  // });
});
