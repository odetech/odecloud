import FileTypes from './types';

export class File implements FileTypes {
  image = '';
  target = '';
  createdBy = '';
  associatedId = '';

  constructor(data: any) {
    if (!data) {
      return;
    }

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload?: FileTypes) => {
      const payload = new FormData();
      payload.append('image', postPayload?.image || this.image);
      payload.append('associatedId', postPayload?.associatedId || this.associatedId);
      payload.append('createdBy', postPayload?.createdBy || this.createdBy);
      payload.append('target', postPayload?.target || this.target);

      return payload;
    },
  });
}
