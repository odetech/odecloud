import {
  post, deleteRequest, get,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getFiles: async (
    searchParams?: {
      associatedId?: string,
      pageNumber?: number,
      limit?: number,
      createdBy?: string,
      updatedBy?: string,
      isStaff?: number,
      isDev?: number,
    },
  ) => {
    try {
      const user = await get(
        getRequestOptions,
        getEndpoint('files'),
        searchParams,
      );
      return user;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getFile: async (
    fileId: string,
    searchParams?: {
      associatedId?: string,
      pageNumber?: number,
      limit?: number,
      createdBy?: string,
      updatedBy?: string,
      isStaff?: number,
      isDev?: number,
    },
  ) => {
    try {
      const user = await get(
        getRequestOptions,
        getEndpoint(`files/${fileId}`),
        searchParams,
      );
      return user;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const bodyType = 'formData';
      const resultId = await post(
        getRequestOptions,
        getEndpoint('files'),
        payload,
        bodyType,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (fileId: string) => {
    try {
      const resultId = await deleteRequest(
        getRequestOptions,
        getEndpoint(`files/${fileId}`),
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
