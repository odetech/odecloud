import { requests } from './requests';
import { File } from './model';
import type FileTypes from './types';

const files = (getRequestOptions: any, getEndpoint: any) => ({
  model: File,
  payload: { ...new File({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  FileTypes,
};

export default files;
