type FileTypes = {
  image: string,
  target: string,
  createdBy: string,
  associatedId: string,
};

export default FileTypes;
