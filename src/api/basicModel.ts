export class BasicModel {
  createdBy: string;
  updatedBy: string;
  createdAt: Date;
  updatedAt: Date;

  constructor(data: any) {
    this.createdBy = '';
    this.updatedBy = '';
    this.createdAt = new Date();
    this.updatedAt = new Date();

    if (!data) {
      return;
    }

    if ('createdAt' in data) {
      this.createdAt = data['createdAt'];
    }

    if ('updatedAt' in data) {
      this.updatedAt = data['updatedAt'];
    }

    if ('createdBy' in data) {
      this.createdBy = data['createdBy'];
      this.updatedBy = data['updatedBy'];
    }

    if ('updatedBy' in data) {
      this.updatedBy = data['updatedBy'];
    }
  }

  setCurrentUserId(userId: string) {
    this.createdBy = userId;
    this.updatedBy = userId;
  }

  isValid() {
    return !!(this.createdBy && this.updatedBy);
  }
}
