import { expect } from 'chai';
import { Message } from './model';

describe('Message\'s Model', () => {
  const MockMessage = () => new Message( {
    createdBy: 'SG8aYLxtAK7cbpakm',
    updatedBy: 'SG8aYLxtAK7cbpakm',
    _id: '2owWTsJA4FRZdzRG4',
    text: '<p>Agree will move this to ongoing.</p>\n',
    rawText: 'Agree will move this to ongoing.',
    tags: [],
    data: [],
    files: [],
  });

  it('checks basic', () => {
    const message = MockMessage();

    expect(message._id).to.equal('2owWTsJA4FRZdzRG4');
    expect(message.text).to.equal('<p>Agree will move this to ongoing.</p>\n');
    expect(message.rawText).to.equal('Agree will move this to ongoing.');
    expect(message.tags.length).to.equal(0);
    expect(message.data.length).to.equal(0);
    expect(message.files.length).to.equal(0);
  });

  describe('payload()', () => {
    const message = MockMessage();

    it('checks getPostPayload()', () => {
      const payload = message.payload().getPostPayload();

      expect(payload.text).to.equal(message.text);
      expect(payload.rawText).to.equal(message.rawText);
      expect(payload.tags.length).to.equal(message.tags.length);
      expect(payload.createdBy).to.equal(message.createdBy);
      expect(payload.updatedBy).to.equal(message.updatedBy);
    });

    it('checks getPatchPayload()', () => {
      const payload = message.payload().getPatchPayload();

      expect(payload.text).to.equal(message.text);
      expect(payload.rawText).to.equal(message.rawText);
    });

    it('checks getFilesPostPayload()', () => {
      const payload = message.payload().getFilesPostPayload();

      expect(payload.message.text).to.equal(message.text);
      expect(payload.message.rawText).to.equal(message.rawText);
      expect(payload.message.tags.length).to.equal(message.tags.length);
      expect(payload.message.createdBy).to.equal(message.createdBy);
      expect(payload.message.updatedBy).to.equal(message.updatedBy);

      expect(payload.files.length).to.equal(message.files.length);
    });
  });
});
