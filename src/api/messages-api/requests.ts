import {
  get, patch, post, deleteRequest,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getMessages: async (searchParams?: {
    associatedId?: string,
    tags?: string[],
    exclude?: string[],
    since?: string,
    isDraft?: boolean,
    isByStaff?: boolean,
    loadBy?: string[],
    data?: string[],
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.tags) {
      delete searchParamsCopy.tags;
    }
    if (searchParamsCopy.exclude) {
      delete searchParamsCopy.exclude;
    }
    if (searchParamsCopy.loadBy) {
      delete searchParamsCopy.loadBy;
    }
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.tags) {
      searchParams.tags.forEach((item) => {
        searchParamsString.append('tags', item);
      });
    }
    if (searchParams?.exclude) {
      searchParams.exclude.forEach((item) => {
        searchParamsString.append('exclude', item);
      });
    }
    if (searchParams?.loadBy) {
      searchParams.loadBy.forEach((item) => {
        searchParamsString.append('loadBy', item);
      });
    }
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint('messages');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString().replace(/%3A/gm,':')}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getMessage: async (messageId: string, searchParams?: {
    origin: string | 'odesocial' | 'odetask' | 'chat',
    associatedId?: string,
    tags?: string[],
    exclude?: string[],
    since?: string,
    isDraft?: boolean,
    isByStaff?: boolean,
    loadBy?: string[],
    data?: string[],
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.tags) {
      delete searchParamsCopy.tags;
    }
    if (searchParamsCopy.exclude) {
      delete searchParamsCopy.exclude;
    }
    if (searchParamsCopy.loadBy) {
      delete searchParamsCopy.loadBy;
    }
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.tags) {
      searchParams.tags.forEach((item) => {
        searchParamsString.append('tags', item);
      });
    }
    if (searchParams?.exclude) {
      searchParams.exclude.forEach((item) => {
        searchParamsString.append('exclude', item);
      });
    }
    if (searchParams?.loadBy) {
      searchParams.loadBy.forEach((item) => {
        searchParamsString.append('loadBy', item);
      });
    }
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint(`messages/${messageId}`);
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString().replace(/%3A/gm,':')}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (
    payload: any,
    additionalParams?: {
      app: string | 'odesocial' | 'odetask',
      isChat: boolean,
    },
  ) => {
    let url = getEndpoint('messages');
    if (additionalParams) {
      const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
      const searchParamsString = new URLSearchParams(searchParamsCopy);

      url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
    }

    try {
      const request = await post(
        getRequestOptions,
        url,
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  search: async (searchParams: {
    search: string,
    associatedId?: string,
    tags?: string[],
    data?: string[],
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.tags) {
      delete searchParamsCopy.tags;
    }
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.tags) {
      searchParams.tags.forEach((item) => {
        searchParamsString.append('tags', item);
      });
    }
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint('messages/search');
    if (searchParams) {
      url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (
    messageId: string,
    additionalParams?: {
      app: string | 'odesocial' | 'odetask',
      isChat: boolean,
    },
  ) => {
    try {
      let url = getEndpoint(`messages/${messageId}`);
      if (additionalParams) {
        const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
        const searchParamsString = new URLSearchParams(searchParamsCopy);

        url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
      }

      const request = await deleteRequest(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (
    messageId: string,
    payload: any,
    additionalParams?: {
      app: string | 'odesocial' | 'odetask',
      isChat: boolean,
    },
  ) => {
    try {
      let url = getEndpoint(`messages/${messageId}`);
      if (additionalParams) {
        const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
        const searchParamsString = new URLSearchParams(searchParamsCopy);

        url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
      }

      const request = await patch(
        getRequestOptions,
        url,
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  postFiles: async (
    payload: any,
    additionalParams?: {
      app: string | 'odesocial' | 'odetask',
      isChat: boolean,
    },
  ) => {
    let url = getEndpoint('messages/files');
    if (additionalParams) {
      const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
      const searchParamsString = new URLSearchParams(searchParamsCopy);

      url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
    }

    try {
      const request = await post(
        getRequestOptions,
        url,
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patchFiles: async (
    messageId: string,
    payload: any,
    additionalParams?: {
      app: string | 'odesocial' | 'odetask',
      isChat: boolean,
    },
  ) => {
    let url = getEndpoint(`messages/${messageId}/files`);
    if (additionalParams) {
      const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
      const searchParamsString = new URLSearchParams(searchParamsCopy);

      url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
    }

    try {
      const request = await patch(
        getRequestOptions,
        url,
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  reactions: {
    // For both PATCH & DELETE API will assume the current authenticated user as the user who will be liking
    // a message. Therefore, a user id is not required but just the message id.
    patch: async (
      messageId: string,
      reaction: string,
      additionalParams?: {
        app: string | 'odesocial' | 'odetask',
      },
    ) => {
      let url = getEndpoint(`messages/${messageId}/${reaction}`);
      if (additionalParams) {
        const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
        const searchParamsString = new URLSearchParams(searchParamsCopy);

        url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
      }

      try {
        const request = await patch(
          getRequestOptions,
          url,
          {},
        );
        return request;
      } catch (e) {
        throw e; // handle error
      }
    },
    delete: async (messageId: string, reaction: string) => {
      try {
        const request = await deleteRequest(
          getRequestOptions,
          getEndpoint(`messages/${messageId}/${reaction}`),
        );
        return request;
      } catch (e) {
        // handle error
        throw e;
      }
    },
  },
  getUnreadMessages: async (
    additionalParams: {
      origin: string | 'odesocial' | 'odetask' | 'chat',
    },
  ) => {
    try {
      let url = getEndpoint('messages/unread');
      if (additionalParams) {
        const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
        const searchParamsString = new URLSearchParams(searchParamsCopy);

        url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
      }
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patchReadMessages: async (
    payload: string[],
    additionalParams: {
      app: string | 'odesocial' | 'odetask' | 'odeserver',
      isChat: boolean,
    },
  ) => {
    try {
      let url = getEndpoint('messages/read');
      if (additionalParams) {
        const searchParamsCopy = additionalParams ? JSON.parse(JSON.stringify(additionalParams)) : {};
        const searchParamsString = new URLSearchParams(searchParamsCopy);

        url = url.concat(`?${searchParamsString.toString().replace(/%3A/gm,':')}`);
      }

      const request = await patch(
        getRequestOptions,
        url,
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
