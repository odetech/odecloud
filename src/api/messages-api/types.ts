type MessageTypes = {
  _id: string,
  associatedId?: string | null,
  data?: string[] | null,
  extra?: any | null,
  files?: any[] | null,
  isByStaff?: boolean,
  isDraft?: boolean,
  likes?: string[],
  fun?: string[],
  helpful?: string[],
  important?: string[],
  insightful?: string[],
  mentions?: string[],
  text: string,
  rawText: string,
  tags: string[],
  title?: string,
  views?: string[],
};

export default MessageTypes;
