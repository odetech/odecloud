import { BasicModel } from '../basicModel';
import MessageTypes from './types';

export class Message extends BasicModel implements MessageTypes {
  _id = '';
  text = '';
  rawText = '';
  tags = []; // array of tag IDs
  data = []; // array of ['createdBy', 'commentsCount', 'bookmark', 'notificationSettings', 'files']
  files = []; // array of Cloudinary Objects

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: () => {
      const payload = {
        text: this.text,
        rawText: this.rawText,
        tags: this.tags,
        createdBy: this.createdBy,
        updatedBy: this.updatedBy,
      };

      return payload;
    },

    getPatchPayload: () => {
      const payload = {
        text: this.text,
        rawText: this.rawText,
      };

      return payload;
    },

    getFilesPostPayload: () => {
      const payload = {
        message: {
          text: this.text,
          rawText: this.rawText,
          tags: this.tags,
          createdBy: this.createdBy,
          updatedBy: this.updatedBy,
        },
        files: this.files,
      };

      return payload;
    },
  });
}
