import { requests } from './requests';
import { Message } from './model';
import type MessageTypes from './types';

const messages = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Message,
    payload: { ...new Message({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  MessageTypes,
};

export default messages;
