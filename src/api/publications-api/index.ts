import { requests } from './requests';
import { Publication } from './model';
import type PublicationTypes from './types';

const publications = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Publication,
    payload: { ...new Publication({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  PublicationTypes,
};

export default publications;
