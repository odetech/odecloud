import { expect } from 'chai';
import { Publication } from './model';

describe('Publication\'s Model', () => {
  const MockPublication = () => new Publication({
    _id: '123456',
    belongTo: '123XYZ',
    articleIdList: [],
    title: 'Hello World',
    description: 'Lorem ipsum dolor sit amet',
  });

  it('checks basic', () => {
    const publication = MockPublication();

    expect(publication._id).to.equal('123456');
    expect(publication.belongTo).to.equal('123XYZ');
    expect(publication.title).to.equal('Hello World');
    expect(publication.description).to.equal('Lorem ipsum dolor sit amet');
    expect(publication.articleIdList.length).to.equal(0);
  });

  describe('payload()', () => {
    const publication = MockPublication();

    it('checks getPostPayload()', () => {
      const payload = publication.payload().getPostPayload();

      expect(payload.title).to.equal(publication.title);
      expect(payload.description).to.equal(publication.description);
      expect(payload.articleIdList.length).to.equal(publication.articleIdList.length);
      expect(new Date(payload.createdAt).getTime() > new Date(publication.createdAt).getTime()).to.equal(true);
      expect(new Date(payload.updatedAt).getTime() > new Date(publication.updatedAt).getTime()).to.equal(true);
      expect(payload.updatedBy).to.equal(publication.updatedBy);
      expect(payload.createdBy).to.equal(publication.createdBy);
    });

    it('checks getPatchPayload()', () => {
      const payload = publication.payload().getPatchPayload();

      expect(payload.title).to.equal(publication.title);
      expect(payload.description).to.equal(publication.description);
      expect(payload.articleIdList).to.equal(publication.articleIdList);
      expect(payload.updatedBy).to.equal(publication.updatedBy);
      expect(payload.updatedAt > new Date(publication.updatedAt)).to.equal(true);
    });

  });
});
