import { BasicModel } from '../basicModel';
import PublicationTypes from './types';

export class Publication extends BasicModel implements PublicationTypes {
  _id = '';
  belongTo = ''; // userId, orgId, etc
  articleIdList = [];
  title = '';
  description = '';

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getPostPayload: () => {
        const payload = {
          title: this.title,
          description: this.description,
          belongTo: this.belongTo,
          articleIdList: this.articleIdList,
          createdBy: this.createdBy,
          updatedBy: this.updatedBy,
          createdAt: new Date(),
          updatedAt: new Date()
        };

        return payload;
      },

      getPatchPayload: (patchPayload?: any) => {
        const payload: any = {
          updatedAt: new Date(),
          updatedBy: this.updatedBy,
        };

        if (patchPayload) {
          if (patchPayload.title) {
            payload.title = patchPayload.title;
          }

          if (patchPayload.description) {
            payload.description = patchPayload.description;
          }

          if (patchPayload.articleIdList) {
            payload.articleIdList = patchPayload.articleIdList;
          }
        } else {
          payload.title = this.title;
          payload.description = this.description;
          payload.articleIdList = this.articleIdList;
        }

        // Checking the number of elements in the payload
        if (Object.keys(payload).length === 2) {
          return null;
        }

        return payload;
      },
    });
  };
}
