import { get, patch, post, deleteRequest } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    get: async (searchParams: any) => {
      try {
        const publications = await get(
          getRequestOptions,
          getEndpoint('publications'),
          searchParams,
        );
        return publications;
      } catch(e) {
        // handle error
        throw e;
      }
    },
    patch: async (publicationId: string, payload: any) => {
      try {
        const resultId = await patch(
          getRequestOptions,
          getEndpoint(`publications/${publicationId}`),
          payload,
        );
        return resultId;
      } catch(e) {
        // handle error
        throw e;
      }
    },
    post: async (payload: any) => {
      try {
        const resultId = await post(
          getRequestOptions,
          getEndpoint('publications'),
          payload,
        );
        return resultId;
      } catch(e) {
        // handle error
        throw e;
      }
    },
    delete: async (publicationId: string) => {
      try {
        const resultId = await deleteRequest(
          getRequestOptions,
          getEndpoint(`publications/${publicationId}`),
        );
        return resultId;
      } catch(e) {
        // handle error
        throw e;
      }
    },
    search: async (searchStr: string) => {
      if (!searchStr) {
        return;
      }

      const payload = {
        "searchStr": searchStr,
      };

      try {
        const resultId = await get(
          getRequestOptions,
          getEndpoint('publications/search'),
          payload,
        );

        return resultId;
      } catch(e) {
        // handle error
        throw e;
      }
    }
  })
};
