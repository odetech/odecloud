type PublicationTypes = {
  _id: string,
  belongTo: string,
  articleIdList: any[] | [],
  title: string,
  description: string,
};

export default PublicationTypes;
