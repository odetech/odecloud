import { requests } from './requests';
import { NotificationSetting } from './model';
import type NotificationSettingsTypes from './types';

const notificationSettings = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: NotificationSetting,
    payload: { ...new NotificationSetting({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  NotificationSettingsTypes,
};

export default notificationSettings;
