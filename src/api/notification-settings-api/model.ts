import { BasicModel } from '../basicModel';
import NotificationSettingTypes from './types';

export class NotificationSetting extends BasicModel implements NotificationSettingTypes {
  _id = '';
  userId = '';
  associatedId = '';
  associatedDB = '';
  currentFrequencies = [];

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getPostPayload: () => {
        const payload = {
          userId: this.userId,
          associatedId: this.associatedId,
          associatedDB: this.associatedDB,
          currentFrequencies: this.currentFrequencies,
          createdAt: new Date(),
          updatedAt: new Date(),
        };

        return payload;
      },
      getPatchPayload: () => {
        const payload = {
          currentFrequencies: this.currentFrequencies,
          updatedAt: new Date(),
        };

        return payload;
      },
    });
  };
}
