import { expect } from 'chai';
import { NotificationSetting } from './model';

describe('Notification Setting\'s Model', () => {
  const MockNotificationSetting = () => new NotificationSetting({
    userId: 'Hello World',
    associatedId: 'Lorem ipsum dolor sit amet',
    associatedDB: 'ASD',
    currentFrequencies: ['A', 'B'],
  });

  it('checks basic', () => {
    const notificationSetting = MockNotificationSetting();

    expect(notificationSetting.userId).to.equal('Hello World');
    expect(notificationSetting.associatedId).to.equal('Lorem ipsum dolor sit amet');
    expect(notificationSetting.associatedDB).to.equal('ASD');
    expect(notificationSetting.currentFrequencies[1]).to.equal('B');
  });

  describe('payload()', () => {
    const notificationSetting = MockNotificationSetting();

    it('checks getPostPayload()', () => {
      const payload = notificationSetting.payload().getPostPayload();

      expect(payload.userId).to.equal(notificationSetting.userId);
      expect(payload.associatedId).to.equal(notificationSetting.associatedId);
      expect(payload.associatedDB).to.equal(notificationSetting.associatedDB);
      expect(payload.currentFrequencies[0]).to.equal(notificationSetting.currentFrequencies[0]);
      expect(new Date(payload.createdAt).getTime() > new Date(notificationSetting.createdAt).getTime()).to.equal(true);
      expect(new Date(payload.updatedAt).getTime() > new Date(notificationSetting.updatedAt).getTime()).to.equal(true);
    });

    it('checks getPatchPayload()', () => {
      const payload = notificationSetting.payload().getPostPayload();

      expect(payload.currentFrequencies[1]).to.equal(notificationSetting.currentFrequencies[1]);
      expect(new Date(payload.updatedAt).getTime() > new Date(notificationSetting.updatedAt).getTime()).to.equal(true);
    });
  });
});
