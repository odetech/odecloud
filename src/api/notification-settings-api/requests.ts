import { get, patch, post } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getNotificationSettings: async (searchParams?: {
    userId?: string,
    associatedId?: string,
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const notificationSettings = await get(
        getRequestOptions,
        getEndpoint('notification-settings'),
        searchParams,
      );
      return notificationSettings;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getNotificationSetting: async (notificationSettingId: string, searchParams?: {
    userId?: string,
    associatedId?: string,
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const notificationSetting = await get(
        getRequestOptions,
        getEndpoint(`notification-settings/${notificationSettingId}`),
        searchParams,
      );
      return notificationSetting;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (notificationSettingId: string, payload: any) => {
    try {
      const resultId = await patch(
        getRequestOptions,
        getEndpoint(`notification-settings/${notificationSettingId}`),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
  post: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('notification-settings'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
});
