type NotificationSettingTypes = {
  _id: string,
  userId: string,
  associatedId: string,
  associatedDB: string,
  currentFrequencies: string[],
};

export default NotificationSettingTypes;
