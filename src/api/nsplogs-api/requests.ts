import {
  get,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getLogs: async (searchParams?: any) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('nsplogs'),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
