import { requests } from './requests';
import type NspLogsTypes from './types';

const nspLogs = (getRequestOptions: any, getEndpoint: any) => ({
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  NspLogsTypes,
};

export default nspLogs;
