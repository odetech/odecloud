type NspLogsTypes = {
  endDate: string | null;
  value: {
    success: boolean;
    env: string;
    message: {
      rawText: string;
    };
    sender: {
      _id: string;
      emails: { address: string }[];
    };
    recipient: {
      _id: string;
      emails: { address: string }[];
    };
  };
  _id: string;
  app: string;
  trigger: string;
  section: string;
  messageId: string;
  updatedAt: string;
};

export default NspLogsTypes;
