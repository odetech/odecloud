import {
  get, patch, post, deleteRequest,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getArticles: async (searchParams?: {
    data?: string[],
    isDraft?: boolean,
    isByStaff?: boolean,
    tags?: string[],
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.tags) {
      delete searchParamsCopy.tags;
    }
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.tags) {
      searchParams.tags.forEach((item) => {
        searchParamsString.append('tags', item);
      });
    }
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint('articles');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const articles = await get(
        getRequestOptions,
        url,
      );
      return articles;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getArticle: async (articleId: string, searchParams?: {
    data?: string[],
    isDraft?: boolean,
    isByStaff?: boolean,
    tags?: string[],
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.tags) {
      delete searchParamsCopy.tags;
    }
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.tags) {
      searchParams.tags.forEach((item) => {
        searchParamsString.append('tags', item);
      });
    }
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint(`articles/${articleId}`);
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const article = await get(
        getRequestOptions,
        url,
      );
      return article;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('articles'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  postFiles: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('articles/files'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (articleId: string) => {
    try {
      const resultId = await deleteRequest(
        getRequestOptions,
        getEndpoint(`articles/${articleId}`),
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (articleId: string, payload: any) => {
    try {
      const resultId = await patch(
        getRequestOptions,
        getEndpoint(`articles/${articleId}`),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patchFiles: async (articleId: string, payload: any) => {
    try {
      const resultId = await patch(
        getRequestOptions,
        getEndpoint(`articles/${articleId}/files`),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  search: async (searchStr: string, tags?: string, createdBy?: string) => {
    if (!searchStr) {
      return;
    }

    // TODO: Separate the request and this payload and test this payload.
    const payload: {
      tags?: string,
      searchStr?: string,
      createdBy?: string,
    } = {
      searchStr,
    };

    if (tags) {
      payload.tags = tags;
    }

    if (createdBy) {
      payload.createdBy = createdBy;
    }

    try {
      const resultId = await get(
        getRequestOptions,
        getEndpoint('articles/search'),
        payload,
      );

      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  likes: {
    // For both PATCH & DELETE API will assume the current authenticated user as the user who will be liking
    // an article. Therefore, a user id is not required but just the article id.
    patch: async (articleId: string) => {
      try {
        const resultId = await patch(
          getRequestOptions,
          getEndpoint(`articles/likes/${articleId}`),
          {},
        );
        return resultId;
      } catch (e) {
        throw e; // handle error
      }
    },
    delete: async (articleId: string) => {
      try {
        const resultId = await deleteRequest(
          getRequestOptions,
          getEndpoint(`articles/likes/${articleId}`),
        );
        return resultId;
      } catch (e) {
        // handle error
        throw e;
      }
    },
  },
  mentions: {
    email: {
      post: async (payload: any) => {
        try {
          const resultId = await post(
            getRequestOptions,
            getEndpoint('articles/mentions/email'),
            payload,
          );
          return resultId;
        } catch (e) {
          // handle error
          throw e;
        }
      },
    },
  },
});
