import { expect } from 'chai';
import { Article } from './model';

describe('Article\'s Model', () => {
  const MockArticle = () => new Article({
    _id: '123456',
    isDraft: false,
    title: 'Hello World',
    content: 'Lorem ipsum dolor sit amet',
    externalUrl: 'String',
    rawContent: 'String',
    tags: [],
    views: [],
    files: [],
    likes: [],
    followers: [],
    mentions: [],
    publishedAt: null,
    publishedNickname: 'String',
    isByStaff: false,
    commentsCount: 10,
  });

  it('checks basic', () => {
    const article = MockArticle();

    expect(article._id).to.equal('123456');
    expect(article.isDraft).to.equal(false);
    expect(article.title).to.equal('Hello World');
    expect(article.content).to.equal('Lorem ipsum dolor sit amet');
    expect(article.externalUrl).to.equal('String');
    expect(article.rawContent).to.equal('String');
    expect(article.commentsCount).to.equal(10);
    expect(0).to.equal(article.followers.length);
    expect(0).to.equal(article.tags.length);
    expect(0).to.equal(article.files.length);
    expect(0).to.equal(article.likes.length);
    expect(0).to.equal(article.views.length);
    expect(0).to.equal(article.mentions.length);
    expect(article.publishedAt).to.equal(null);
    expect(article.publishedNickname).to.equal('String');
    expect(article.isByStaff).to.equal(false);
  });

  describe('payload()', () => {
    const article = MockArticle();

    it('checks getPostPayload()', () => {
      const payload = article.payload().getPostPayload();

      expect(payload.title).to.equal(article.title);
      expect(payload.content).to.equal(article.content);
      expect(payload.tags).to.equal(article.tags);
      expect(payload.views).to.equal(article.views);
      expect(payload.likes.length).to.equal(article.likes.length);
      expect(payload.followers).to.equal(article.followers);
      expect(payload.files).to.equal(article.files);
      expect(payload.mentions.length).to.equal(article.mentions.length);
      expect(payload.isDraft).to.equal(article.isDraft);
      expect(payload.publishedAt).to.equal(article.publishedAt);
      expect(payload.updatedBy).to.equal(article.updatedBy);
      expect(payload.createdBy).to.equal(article.createdBy);
    });

    it('checks getSendEmailMentionPayload()', () => {
      const payload = article.payload().getSendEmailMentionPayload({
        title: 'asd'
      });

      expect(payload.title).to.equal('asd');
      expect(payload.content).to.equal(article.content);
      expect(payload.tags).to.equal(article.tags);
      expect(payload.views).to.equal(article.views);
      expect(payload.likes.length).to.equal(article.likes.length);
      expect(payload.followers).to.equal(article.followers);
      expect(payload.files).to.equal(article.files);
      expect(payload.mentions.length).to.equal(article.mentions.length);
      expect(payload.isDraft).to.equal(article.isDraft);
      expect(payload.updatedBy).to.equal(article.updatedBy);
      expect(payload.createdBy).to.equal(article.createdBy);
    });

  });
});
