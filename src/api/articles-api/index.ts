import { requests } from './requests';
import { Article } from './model';
import type ArticleTypes from './types';

const articles = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Article,
    payload: { ...new Article({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  ArticleTypes,
};

export default articles;
