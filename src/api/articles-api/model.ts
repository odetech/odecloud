import { BasicModel } from '../basicModel';
import ArticleTypes from './types';

export class Article extends BasicModel implements ArticleTypes {
  _id = '';
  isDraft = true;
  title = '';
  content = '';
  externalUrl = ''; // If the article is actually a link to an external articles written on Medium, etc
  rawContent = '';
  tags = []; // string of tag._id
  files = []; // list of cloudinary object or file._id
  likes = []; // string of user._id
  views = []; // string of user._id
  data = []; // array of ['createdBy', 'notificationSettings', 'bookmark', 'files']
  followers = []; // string of user._id
  publishedAt = null;
  publishedNickname = '';
  isByStaff = false;
  mentions = [];
  commentsCount = 0;

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getPostPayload: () => {
        const payload = {
          data: this.data,
          title: this.title,
          content: this.content,
          isDraft: this.isDraft,
          rawContent: this.rawContent,
          isByStaff: this.isByStaff,
          externalUrl: this.externalUrl,
          publishedNickname: this.publishedNickname,
          tags: this.tags,
          likes: this.likes,
          files: this.files,
          views: this.views,
          followers: this.followers,
          publishedAt: this.publishedAt,
          createdBy: this.createdBy,
          updatedBy: this.updatedBy,
          mentions: this.mentions,
        };

        return payload;
      },
      getSendEmailMentionPayload: (emailMentionPayload: any) => {
        const payload: {
          title?: string,
          content?: string,
          rawContent?: string,
          externalUrl?: string,
          publishedNickname?: string,
          createdBy?: string,
          updatedBy?: string,
          isDraft?: boolean,
          isByStaff?: boolean,
          mentions?: string[],
          tags?: string[],
          views?: string[],
          files?: string[],
          likes?: string[],
          followers?: string[],
          commentsCount?: number,
          extra?: any,
          data?: any,
        } = {
          title: this.title,
          content: this.content,
          rawContent: this.rawContent,
          externalUrl: this.externalUrl,
          publishedNickname: this.publishedNickname,
          createdBy: this.createdBy,
          updatedBy: this.updatedBy,
          isDraft: this.isDraft,
          isByStaff: this.isByStaff,
          mentions: this.mentions,
          tags: this.tags,
          views: this.views,
          files: this.files,
          likes: this.likes,
          followers: this.followers,
        };

        const keys = Object.keys(emailMentionPayload);
        keys.forEach((key: string) => {
          if (emailMentionPayload[key] !== undefined) {
            payload[key] = emailMentionPayload[key];
          }
        });

        return payload;
      },
    });
  };
}
