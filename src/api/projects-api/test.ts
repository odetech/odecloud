import { expect } from 'chai';
import { Project } from './model';

describe('Project\'s Model', () => {
  const MockProject = () => new Project({
    _id: '123456',
    title: 'qwe',
    description: 'asd',
    gigId: 'zxc',
    orgId: 'wer',
    isActive: false,
    isDemo: true,
    assignedToUserId: ['qwe'],
    pendingUserId: ['asd'],
    startDate: new Date(),
    dueDate: new Date(),
  });

  it('checks basic', () => {
    const project = MockProject();

    expect(project._id).to.equal('123456');
    expect(project.title).to.equal('qwe');
    expect(project.description).to.equal('asd');
    expect(project.gigId).to.equal('zxc');
    expect(project.orgId).to.equal('wer');
    expect(project.isActive).to.equal(false);
    expect(project.isDemo).to.equal(true);
    expect(project.assignedToUserId[0]).to.equal('qwe');
    expect(project.pendingUserId[0]).to.equal('asd');
  });

  describe('payload()', () => {
    const project = MockProject();

    it('checks getPostPayload()', () => {
      const payload = project.payload().getPostPayload({
        title: 'qwe',
        orgId: 'asd',
        assignedToUserId: ['asd'],
      });

      expect(payload.title).to.equal('qwe');
      expect(payload.orgId).to.equal('asd');
      expect(payload.assignedToUserId[0]).to.equal('asd');
    });

    it('checks getPatchPayload()', () => {
      const payload = project.payload().getPatchPayload({
        title: 'sdf',
        isDemo: true,
      });

      expect(payload.title).to.equal('sdf');
      expect(payload.isDemo).to.equal(true);
    });

  });
});
