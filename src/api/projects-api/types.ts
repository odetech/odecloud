type ProjectTypes = {
  _id: string,
  title: string,
  description: string,
  gigId: string,
  orgId: string,
  isActive: boolean,
  isDemo: boolean,
  assignedToUserId: string[],
  pendingUserId: string[],
  startDate: Date,
  dueDate: Date,
};

export default ProjectTypes;
