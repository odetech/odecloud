import { requests } from './requests';
import { Project } from './model';
import type ProjectTypes from './types';

const projects = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Project,
    payload: { ...new Project({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  ProjectTypes,
};

export default projects;
