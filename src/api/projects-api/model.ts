import { BasicModel } from '../basicModel';
import ProjectTypes from './types';

export class Project extends BasicModel implements ProjectTypes {
  _id = '';
  title = '';
  description = '';
  gigId = '';
  orgId = '';
  isActive = true;
  isDemo = false;
  assignedToUserId = [];
  pendingUserId = [];
  startDate = new Date();
  dueDate = new Date();

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload: any) => {
      const payload: {
        title: string,
        description?: string,
        gigId?: string,
        orgId: string,
        isActive?: boolean,
        isDemo?: boolean,
        assignedToUserId: string[],
        pendingUserId?: string[],
        startDate?: Date,
        dueDate?: Date,
      } = {
        title: '',
        orgId: '',
        assignedToUserId: [],
      };

      const keys = Object.keys(postPayload);
      keys.forEach((key: string) => {
        if (postPayload[key] !== undefined) {
          payload[key] = postPayload[key];
        }
      });

      return payload;
    },
    getPatchPayload: (patchPayload: any) => {
      const payload: {
        title?: string,
        description?: string,
        gigId?: string,
        orgId?: string,
        isActive?: boolean,
        isDemo?: boolean,
        assignedToUserId?: string[],
        pendingUserId?: string[],
        startDate?: Date,
        dueDate?: Date,
      } = {};

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
  });
}
