import {
  get, patch, post, deleteRequest,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getMagic: async (body?: {
    pageNumber?: number,
    limit?: number,
    assignedToUserId?: string,
    orgId?: string,
    projectId?: string,
  }) => {
    try {
      const searchParams: Record<string, string | string [] | number> = {
        taskPageNumber: body?.pageNumber || 1,
        taskLimit: body?.limit || 10,
        assignedToUserId: body?.assignedToUserId,
        orgId: body?.orgId,
        _id: body.projectId?.length > 1 ? undefined : body.projectId,
      };

      const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
      const searchParamsString = new URLSearchParams(searchParamsCopy);
      let url = getEndpoint('projects');
      if (searchParams) {
        url = url.concat(`/magic/?${searchParamsString.toString()}`);
      }
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getProjects: async (searchParams?: {
    orgId?: string,
    gigId?: string,
    assignedTouserId?: string,
    isActive?: boolean,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
    _id: string[],
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy._id) {
      delete searchParamsCopy._id;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?._id) {
      searchParams._id.forEach((item) => {
        searchParamsString.append('_id', item);
      });
    }

    let url = getEndpoint('projects');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getProject: async (projectId: string, searchParams?: {
    orgId?: string,
    gigId?: string,
    assignedTouserId?: string,
    isActive?: boolean,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`projects/${projectId}`),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint('projects'),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (projectId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`projects/${projectId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  archive: async (projectId: string) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`projects/${projectId}/archive`),
        {},
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
