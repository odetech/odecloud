import {
  post,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  login: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('login'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  signup: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('signup'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  passwordlessRequest: async (payload: {
    email: string,
    appUrl: string,
    authenticationMethod?: string,
  }) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('passwordless/request'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  passwordlessLogin: async (payload: {
    secret: string,
    when: string,
  }) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('passwordless/login'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  passwordUpdate: async (payload: {
    currentPassword: string,
    newPassword: string,
  }) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('auth/update-password'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  forgotPasswordRequest: async (payload: {
    email: string,
    appUrl: string,
    redirectLink?: string,
    authenticationMethod?: string,
  }) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('forgot-password/request'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  forgotPasswordAccept: async (payload: {
    secret: string,
    when: string,
    newPassword: string,
  }) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('forgot-password/accept'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  resync: async (payload: {
    authToken?: string,
    userId?: string,
    clientKey?: string,
    clientSecret?: string,
    url: string,
  }) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('resync'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
