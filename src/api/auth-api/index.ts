import { requests } from './requests';
import { Auth } from './model';
import type AuthTypes from './types';

const auths = (getRequestOptions: any, getEndpoint: any) => ({
  model: Auth,
  payload: { ...new Auth({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  AuthTypes,
};

export default auths;
