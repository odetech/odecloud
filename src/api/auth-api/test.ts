import { expect } from 'chai';
import { Auth } from './model';

describe('Auth\'s Model', () => {
  const MockAuth = () => new Auth({
    email: '123',
    password: 'Hello World',
    firstName: 'Lorem ipsum dolor sit amet',
    lastName: 'String',
  });

  it('checks basic', () => {
    const auth = MockAuth();

    expect(auth.email).to.equal('123');
    expect(auth.password).to.equal('Hello World');
    expect(auth.firstName).to.equal('Lorem ipsum dolor sit amet');
    expect(auth.lastName).to.equal('String');
  });

  describe('payload()', () => {
    const auth = MockAuth();

    it('checks getLoginPayload()', () => {
      const payload = auth.payload().getLoginPayload();

      expect(payload.email).to.equal('123');
      expect(payload.password).to.equal('Hello World');
    });

    it('checks getSignupPayload()', () => {
      const payload = auth.payload().getSignupPayload();

      expect(payload.email).to.equal('123');
      expect(payload.password).to.equal('Hello World');
      expect(payload.firstName).to.equal('Lorem ipsum dolor sit amet');
      expect(payload.lastName).to.equal('String');
    });

    it('checks getPasswordUpdatePayload()', () => {
      const payload = auth.payload().getPasswordUpdatePayload({
        currentPassword: '123',
        newPassword: '234',
      });

      expect(payload.currentPassword).to.equal('123');
      expect(payload.newPassword).to.equal('234');
    });
  });
});
