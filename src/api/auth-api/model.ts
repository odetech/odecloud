import AuthTypes from './types';

export class Auth implements AuthTypes {
  email = '';
  password = '';
  firstName = '';
  lastName = '';

  constructor(data: any) {
    if (!data) {
      return;
    }

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getLoginPayload: (loginPayload?: AuthTypes) => {
      const payload = {
        email: loginPayload?.email || this.email,
        password: loginPayload?.password || this.password,
      };

      return payload;
    },
    getSignupPayload: (postPayload?: AuthTypes) => {
      const payload = {
        email: postPayload?.email || this.email,
        password: postPayload?.password || this.password,
        firstName: postPayload?.firstName || this.firstName,
        lastName: postPayload?.lastName || this.lastName,
      };

      return payload;
    },
    getPasswordUpdatePayload: (passwordUpdatePayload?: {
      currentPassword: string,
      newPassword: string,
    }) => {
      const payload = {
        currentPassword: passwordUpdatePayload?.currentPassword || '',
        newPassword: passwordUpdatePayload?.newPassword || '',
      };

      return payload;
    },
  });
}
