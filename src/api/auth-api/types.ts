type AuthTypes = {
  email: string,
  password: string,
  firstName?: string,
  lastName?: string,
};

export default AuthTypes;
