import { get, patch, post } from '../../base-requests';
import externals from "./index";

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getExternalsProfiles: async (
    searchParams?: {
      search?: string,
      searchBy?: string,
      server_token?: string,
      pageNumber?: number,
      limit?: number,
      createdBy?: string,
      updatedBy?: string,
    },
    headers?: {
      token?: string,
    },
  ) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('externals/profiles'),
        searchParams,
        headers,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getExternalsProfile: async (
    id: string,
    searchParams?: {
      search?: string,
      searchBy?: string,
      server_token?: string,
      pageNumber?: number,
      limit?: number,
      createdBy?: string,
      updatedBy?: string,
    },
    headers?: {
      token?: string,
    },
  ) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`externals/profiles/${id}`),
        searchParams,
        headers,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
