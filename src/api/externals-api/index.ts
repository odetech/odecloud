import { requests } from './requests';
import { Externals } from './model';

const externals = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Externals,
    ...requests(getRequestOptions, getEndpoint),
  })
}

export default externals;
