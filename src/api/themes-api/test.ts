import { expect } from "chai";
import { Theme } from "./model";

describe("Tag's Model", () => {
  const MockTheme = () =>
    new Theme({
      brand: {
        primary: "primaryCode",
      },
      text: {
        onSurface: "onSurfaceCode",
      },
      background: {
        surface: "surfaceCode",
      },
      info: {
        onPrimary: "onPrimaryCode",
      },
      success: {
        onPrimaryContainer: "successOnPrimaryContainer",
      },
      error: {
        onPrimaryContainer: "errorOnPrimaryContainer",
      },
      outline: {
        100: "outline100",
      },
      visualisation: {
        black: "blackVisualisation",
      },
      gradient: {
        orange: {
          start: "startOrange",
          end: "endOrange",
        },
      },
      shadow: {
        drop: {
          1: "shadow drop 1",
          2: "shadow drop 2",
          3: "shadow drop 3",
        },
        inner: {
          1: "shadow inner 1",
        },
      },
    });

  it("checks basic", () => {
    const theme = MockTheme();
    expect(theme.brand.primary).to.equal("primaryCode");
    expect(theme.text.onSurface).to.equal("onSurfaceCode");
    expect(theme.background.surface).to.equal("surfaceCode");
    expect(theme.info.onPrimary).to.equal("onPrimaryCode");
    expect(theme.success.onPrimaryContainer).to.equal(
      "successOnPrimaryContainer"
    );
    expect(theme.error.onPrimaryContainer).to.equal("errorOnPrimaryContainer");
    expect(theme.outline[100]).to.equal("outline100");
    expect(theme.visualisation.black).to.equal("blackVisualisation");
    expect(theme.gradient.orange.start).to.equal("startOrange");
    expect(theme.gradient.orange.end).to.equal("endOrange");

    expect(theme.shadow.drop[1]).to.equal("shadow drop 1");
  });
});
