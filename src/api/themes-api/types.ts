type ThemeTypes = {
  brand: {
    primary: string;
    primaryContainer: string;
    primaryContainerVar: string;
    secondary: string;
    secondaryContainer: string;
    secondaryContainerSecond: string;
  };
  text: {
    onSurface: string;
    onSurfaceVar: string;
    onSurfaceVarSecond: string;
    onSurfaceDisabled: string;
    onPrimary: string;
    onSecondary: string;
  };
  background: {
    surface: string;
    surfaceContainer: string;
    surfaceContainerVar: string;
    surfaceContainerHigh: string;
    surfaceContainerHighest: string;
  };
  success: {
    primary: string;
    onPrimary: string;
    primaryContainer: string;
    onPrimaryContainer: string;
  };
  error: {
    primary: string;
    onPrimary: string;
    primaryContainer: string;
    onPrimaryContainer: string;
  };
  info: {
    primary: string;
    onPrimary: string;
    primaryContainer: string;
    onPrimaryContainer: string;
  };
  outline: {
    100: string;
    200: string;
    300: string;
  };
  visualisation: {
    white: string;
    black: string;
    purple: string;
    yellow: string;
    warmRed: string;
    blueSky: string;
    gray: string;
    pink: string;
    turquoiseGreen: string;
    brown: string;
    raspberry: string;
    diodeBlue: string;
    greenGrass: string;
    terracotta: string;
    lilac: string;
    teal: string;
    cherry: string;
  };
  gradient: {
    orange: {
      start: string;
      end: string;
    };
    purple: {
      start: string;
      end: string;
    };
    green: {
      start: string;
      end: string;
    };
    pink: {
      start: string;
      end: string;
    };
    blue: {
      start: string;
      end: string;
    };
  };
  shadow: {
    drop: {
      1: string;
      2: string;
      3: string;
    };
    inner: {
      1: string;
    };
  };
};

export default ThemeTypes;
