import { get } from "../../base-requests";

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getThemes: async () => {
    let url = getEndpoint("themes");

    try {
      const request = await get(getRequestOptions, url);
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
