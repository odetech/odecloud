import { requests } from './requests';
import { Theme } from './model';
import type ThemeTypes from "./types";

const themes = (getRequestOptions: any, getEndpoint: any) => {
  return {
    model: Theme,
    payload: { ...new Theme({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  };
}

export type { ThemeTypes };

export default themes;
