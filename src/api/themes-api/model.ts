import { BasicModel } from "../basicModel";
import TagTypes from "./types";

export class Theme implements TagTypes {
  brand = {
    primary: "",
    primaryContainer: "",
    primaryContainerVar: "",
    secondary: "",
    secondaryContainer: "",
    secondaryContainerSecond: "",
  };

  text = {
    onSurface: "",
    onSurfaceVar: "",
    onSurfaceVarSecond: "",
    onSurfaceDisabled: "",
    onPrimary: "",
    onSecondary: "",
  };

  background = {
    surface: "",
    surfaceContainer: "",
    surfaceContainerVar: "",
    surfaceContainerHigh: "",
    surfaceContainerHighest: "",
  };

  success = {
    primary: "",
    onPrimary: "",
    primaryContainer: "",
    onPrimaryContainer: "",
  };

  error = {
    primary: "",
    onPrimary: "",
    primaryContainer: "",
    onPrimaryContainer: "",
  };

  info = {
    primary: "",
    onPrimary: "",
    primaryContainer: "",
    onPrimaryContainer: "",
  };

  outline = {
    100: "",
    200: "",
    300: "",
  };

  visualisation = {
    white: "",
    black: "",
    purple: "",
    yellow: "",
    warmRed: "",
    blueSky: "",
    gray: "",
    pink: "",
    turquoiseGreen: "",
    brown: "",
    raspberry: "",
    diodeBlue: "",
    greenGrass: "",
    terracotta: "",
    lilac: "",
    teal: "",
    cherry: "",
  };

  gradient = {
    orange: { start: "", end: "" },
    purple: { start: "", end: "" },
    green: { start: "", end: "" },
    pink: { start: "", end: "" },
    blue: { start: "", end: "" },
  };

  shadow = {
    drop: {
      1: "",
      2: "",
      3: "",
    },
    inner: {
      1: "",
    },
  };

  constructor(data: any) {
    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({});
}

export default Theme;
