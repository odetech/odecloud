import {
  deleteRequest, get, patch, post,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getUsers: async (searchParams?: {
    avatar?: number,
    skillsets?: number,
    roles?: number,
    limit?: number,
    isStaff?: number,
    isDev?: number,
    masterNotifications?: number,
    appStore?: number,
    _id?: string[]
    expertise?: number,
  }) => {
    try {
      const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
      if (searchParamsCopy._id) {
        delete searchParamsCopy._id;
      }
      const searchParamsString = new URLSearchParams(searchParamsCopy);

      if (searchParams?._id) {
        searchParams._id.forEach((item) => {
          searchParamsString.append('_id', item);
        });
      }

      let url = getEndpoint('users');
      if (searchParams) {
        url = url.concat(`/?${searchParamsString.toString()}`);
      }

      const users = await get(
        getRequestOptions,
        url,
      );
      return users;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getUser: async (userId: string, searchParams?: {
    avatar?: number,
    skillsets?: number,
    roles?: number,
    limit?: number,
    isStaff?: number,
    isDev?: number,
    masterNotifications?: number,
    expertise?: number,
  }) => {
    try {
      const user = await get(
        getRequestOptions,
        getEndpoint(`users/${userId}`),
        searchParams,
      );
      return user;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (userId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`users/${userId}/profile`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patchTimeZone: async (userId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`users/${userId}/timezone`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  createUserAvatar: async (userId: string, payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint(`users/${userId}/avatar`),
        payload,
        'formData',
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  deleteUserAvatar: async (userId: string) => {
    try {
      const resultId = await deleteRequest(
        getRequestOptions,
        getEndpoint(`users/${userId}/avatar`),
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getOdeProfilesUsers: async (
    searchParams?: {
      search?: string,
      searchBy?: string,
      server_token?: string,
      pageNumber?: number,
      limit?: number,
      createdBy?: string,
      updatedBy?: string,
    },
  ) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('users/odeprofiles'),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getOdeProfilesUser: async (
    id: string,
    searchParams?: {
      search?: string,
      searchBy?: string,
      server_token?: string,
      pageNumber?: number,
      limit?: number,
      createdBy?: string,
      updatedBy?: string,
    },
  ) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`users/odeprofiles/${id}`),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patchRoles: async (userId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`users/${userId}/roles`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patchFrequencies: async (app: string, frequency: any, searchParams: any) => {
    try {
      const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
      if (searchParamsCopy._id) {
        delete searchParamsCopy._id;
      }
      const searchParamsString = new URLSearchParams(searchParamsCopy);

      let url = getEndpoint(`users/${app}/${frequency}`);
      if (searchParams) {
        url = url.concat(`/?${searchParamsString.toString()}`);
      }
      const resultId = await patch(
        getRequestOptions,
        url,
        searchParams,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
  muteNotifications: async (status: any, searchParams: any) => {
    try {
      const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
      if (searchParamsCopy._id) {
        delete searchParamsCopy._id;
      }
      const searchParamsString = new URLSearchParams(searchParamsCopy);

      let url = getEndpoint(`users/notifications/${status}`);
      if (searchParams) {
        url = url.concat(`/?${searchParamsString.toString()}`);
      }
      const resultId = await patch(
        getRequestOptions,
        url,
        searchParams,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
});
