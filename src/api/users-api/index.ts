import { requests } from './requests';
import { User } from './model';
import type { UserProfileTypes, UserTypes } from './types';

const users = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: User,
    payload: { ...new User({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  UserProfileTypes,
  UserTypes,
};

export default users;
