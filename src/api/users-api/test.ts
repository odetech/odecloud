import { expect } from 'chai';
import { User } from './model';

describe('User\'s Model', () => {
  const MockUser = () => new User({
    "_id": "XXX",
    "createdAt": "2019-02-18T22:35:54.257Z",
    "emails": [{ "address": "vanielle@odecloud.com", "verified": true }],
    "username": "leevanielle",
    "profile": {
      "firstName": "Vanielle",
      "lastName": "Lee",
      "tagline": "CTO - Software is my passion",
      "aboutMe": "Software Engineer",
      "avatar": {
        "_id": "XXX",
        "secureUrl": "ZZZ"
      },
      "skillsets": null,
      "roles": {
        "_id": "XXX",
        "isStaff": true
      }
    },
  });

  it('checks basic', () => {
    const user = MockUser();

    expect(user._id).to.equal('XXX');
    expect(user.emails[0].address).to.equal('vanielle@odecloud.com');
    expect(new Date(user.createdAt).getFullYear()).to.equal(2019);
    expect(user.username).to.equal('leevanielle');
    expect(user.profile.firstName).to.equal('Vanielle');
    expect(user.profile.lastName).to.equal('Lee');
    expect(user.profile.tagline).to.equal('CTO - Software is my passion');
    expect(user.profile.aboutMe).to.equal('Software Engineer');
    expect(user.profile.avatar._id).to.equal('XXX');
    expect(user.profile.avatar.secureUrl).to.equal('ZZZ');
    expect(user.profile.skillsets).to.equal(null);
    expect(user.profile.roles._id).to.equal('XXX');
    expect(user.profile.roles.isStaff).to.equal(true);
  });

  it('checks user.getName()', () => {
    const user = MockUser();
    expect(user.getName()).to.equal('Vanielle Lee');
  });

  it('checks user.getAvatarUrl()', () => {
    const user = MockUser();
    expect(user.getAvatarUrl()).to.equal('ZZZ');
  });

  it('checks getPatchPayload()', () => {
    const payload = MockUser().payload().getPatchPayload({
      firstName: 'asd',
      lastName: 'qwe',
      tagline: 'wer',
      aboutMe: 'sdf',
    });
    expect(payload.firstName).to.equal('asd');
    expect(payload.lastName).to.equal('qwe');
    expect(payload.tagline).to.equal('wer');
    expect(payload.aboutMe).to.equal('sdf');
  });
});
