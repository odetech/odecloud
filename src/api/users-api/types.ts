type UserProfileTypes = {
  firstName: string,
  lastName: string,
  createdBy: string,
  tagline: string,
  aboutMe: string,
  createdFrom: string,
  avatar: null | {
    secureUrl: string,
    _id: string,
  },
  skillsets: any | null,
  roles: any | null,
};

type UserTypes = {
  _id: string,
  createdAt: string | null,
  emails: { address: string, verified: boolean }[] | [],
  profile: UserProfileTypes | null,
  username: string,
  service?: any | null,
  password?: string | null,
};

export {
  UserProfileTypes,
  UserTypes,
};
