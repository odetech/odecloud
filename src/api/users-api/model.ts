import { UserProfileTypes, UserTypes } from './types';

export class UserProfile implements UserProfileTypes {
  firstName = '';
  lastName = '';
  createdBy = '';
  tagline = '';
  aboutMe = '';
  createdFrom = 'PLATFORM'; // Can be 'SLACK', 'GOOGLE CHROME EXTENSION', ETC
  avatar = null;
  skillsets = null;
  roles = null;

  constructor(data: any) {
    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }
}

export class User implements UserTypes {
  _id = '';
  createdAt = null;
  emails = [];
  profile = new UserProfile({});
  service = null;
  username = '';
  password = '';

  constructor(data: any) {
    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        if (key === 'profile') {
          (this as any)[key] = new UserProfile(data['profile']);
        } else {
          (this as any)[key] = data[key];
        }
      }
    });
  }

  addRoles(roles: any) {
    this.profile.roles = roles ? roles : null;
  }

  addAvatar(avatar: any) {
    this.profile.avatar = avatar ? avatar : null;
  }

  addSkilltset(skillsets: any) {
    this.profile.skillsets = skillsets ? skillsets : null;
  }

  getName() {
    if ( this.profile['firstName'] == '' || this.profile['lastName'] == '' ) {
      return `@${this.username}`;
    }

    const name = [];

    if ('firstName' in this.profile) {
      name.push(this.profile['firstName']);
    }

    if ('lastName' in this.profile) {
      name.push(this.profile['lastName']);
    }

    return name.join(' ');
  }

  getAvatarUrl() {
    if (!this.profile.avatar) {
      return '';
    }

    return this.profile.avatar.secureUrl;
  }

  payload = () => ({
    getInsertPayload: () => {
      const payload = {
        'email': this.emails[0].address,
        'password': this.password,
        'profile': this.profile
      };
      return payload;
    },
    getPatchPayload: (patchPayload?: any) => {
      const payload: {
        firstName?: string,
        lastName?: string,
        tagline?: string,
        aboutMe?: string,
      } = {};

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
  });
}
