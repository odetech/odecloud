import { expect } from 'chai';
import { Note } from './model';

describe('Note\'s Model', () => {
  const MockNote = () => new Note({
    _id: '123456',
    value: 'Hello World',
    rawValue: 'qwe',
    associatedId: 'asd',
  });

  it('checks basic', () => {
    const note = MockNote();

    expect(note._id).to.equal('123456');
    expect(note.value).to.equal('Hello World');
    expect(note.rawValue).to.equal('qwe');
    expect(note.associatedId).to.equal('asd');
  });

  describe('payload()', () => {
    const note = MockNote();

    it('checks getPostPayload()', () => {
      const payload = note.payload().getPostPayload({
        value: 'wer',
        rawValue: 'sdf',
        associatedId: 'xcv',
      });

      expect(payload.value).to.equal('wer');
      expect(payload.rawValue).to.equal('sdf');
      expect(payload.associatedId).to.equal('xcv');
    });

    it('checks getPatchPayload()', () => {
      const payload = note.payload().getPatchPayload({
        value: 'xcv',
        rawValue: 'wer',
        associatedId: 'sdf',
      });

      expect(payload.value).to.equal('xcv');
      expect(payload.rawValue).to.equal('wer');
      expect(payload.associatedId).to.equal('sdf');
    });

  });
});
