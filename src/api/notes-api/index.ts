import { requests } from './requests';
import { Note } from './model';
import type NoteTypes from './types';

const notes = (getRequestOptions: any, getEndpoint: any) => ({
  model: Note,
  payload: { ...new Note({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  NoteTypes,
};

export default notes;
