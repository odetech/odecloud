type NoteTypes = {
  _id: string,
  value: string,
  rawValue: string,
  associatedId: string,
};

export default NoteTypes;
