import {
  get, patch, post, deleteRequest,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getNotes: async (searchParams?: {
    associatedId?: string,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint('notes'),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getNote: async (noteId: string, searchParams?: {
    associatedId?: string,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const request = await get(
        getRequestOptions,
        getEndpoint(`notes/${noteId}`),
        searchParams,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint('notes'),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (noteId: string) => {
    try {
      const request = await deleteRequest(
        getRequestOptions,
        getEndpoint(`notes/${noteId}`),
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (noteId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`notes/${noteId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
