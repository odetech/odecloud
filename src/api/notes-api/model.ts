import { BasicModel } from '../basicModel';
import NoteTypes from './types';

export class Note extends BasicModel implements NoteTypes {
  _id = '';
  value = '';
  rawValue = '';
  associatedId = '';

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload: any) => {
      const payload: {
        value: string,
        rawValue: string,
        associatedId: string,
      } = {
        value: '',
        rawValue: '',
        associatedId: '',
      };

      const keys = Object.keys(postPayload);
      keys.forEach((key: string) => {
        if (postPayload[key] !== undefined) {
          payload[key] = postPayload[key];
        }
      });

      return payload;
    },
    getPatchPayload: (patchPayload: any) => {
      const payload: {
        value: string,
        rawValue: string,
        associatedId: string,
      } = {
        value: '',
        rawValue: '',
        associatedId: '',
      };

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
  });
}
