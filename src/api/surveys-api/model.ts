import SurveyTypes from './types';
import { BasicModel } from '../basicModel';

export class Survey extends BasicModel implements SurveyTypes {
  questions = [];
  where = '';
  _id = '';

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload?: SurveyTypes) => {
      const payload = {
        questions: postPayload?.questions || this.questions,
        where: postPayload?.where || this.where,
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      return payload;
    },
  });
}
