import { expect } from 'chai';
import { Survey } from './model';

describe('Survey\'s Model', () => {
  const MockSurvey = () => new Survey({
    _id: 'DtpAnwgoTnt57g9ht',
    questions: ['123', '234'],
    where: '456',
  });

  it('checks basic', () => {
    const survey = MockSurvey();

    expect(survey._id).to.equal('DtpAnwgoTnt57g9ht');
    expect(survey.questions[0]).to.equal('123');
    expect(survey.where).to.equal('456');
  });

  describe('payload()', () => {
    const survey = MockSurvey();

    it('checks getPostPayload()', () => {
      const payload = survey.payload().getPostPayload({
        questions: ['456', '567'],
        where: '123',
      });

      expect(payload.questions[0]).to.equal('456');
      expect(payload.where).to.equal('123');
    });
  });
});
