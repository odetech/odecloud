type SurveyTypes = {
  _id?: string,
  questions: string[],
  where: string,
  createdBy?: string;
  updatedBy?: string,
  createdAt?: Date | null,
  updatedAt?: Date | null,
};

export default SurveyTypes;
