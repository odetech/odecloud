import { requests } from './requests';
import { Survey } from './model';
import type SurveyTypes from './types';

const surveys = (getRequestOptions: any, getEndpoint: any) => ({
  model: Survey,
  payload: { ...new Survey({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  SurveyTypes,
};

export default surveys;
