import { get, post } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getSurveys: async (searchParams?: {
    target?: string,
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const surveys = await get(
        getRequestOptions,
        getEndpoint('surveys'),
        searchParams,
      );
      return surveys;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getSurvey: async (surveyId: string, searchParams?: {
    target?: string,
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const survey = await get(
        getRequestOptions,
        getEndpoint(`surveys/${surveyId}`),
        searchParams,
      );
      return survey;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('surveys'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
