import { actions } from './actions';

const websocket = (getRequestOptions: any, getEndpoint: any) => ({
  ...actions(getRequestOptions, getEndpoint),
});

export default websocket;
