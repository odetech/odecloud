import { post } from '../../base-requests';

let authData: {
  endPoint: string,
  userId: string,
  hashedToken: string,
  origin: string,
  when: string,
};
let url: string | null;
let messageAction: (data: string) => void | undefined;
let pingAction: (data: string) => void | undefined;
let dataAction: (data: string) => void | undefined;
let errorAction: (data: string) => void | undefined;

export const actions = (getRequestOptions: any, getEndpoint: any) => ({
  auth: async (payload: {
    userId: string,
    endPoint: string,
  }) => {
    try {
      const authRequest = await post(
        getRequestOptions,
        getEndpoint('auth/websocket'),
        null,
      );
      authData = authRequest;
      authData.userId = payload.userId;
      authData.endPoint = payload.endPoint;
      return authData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  closeConnection: (websocket: any) => {
    websocket.close(1000, 'Close connection');
    url = null;
  },
  chats: (tagIdArray?: string[], websocket?: any) => {
    if (tagIdArray) {
      url = `wss://${authData.endPoint}/tags/listen?hashedToken=${authData.hashedToken}&origin=${authData.origin}&when=${authData.when}&userId=${authData.userId}`;

      tagIdArray.forEach((tagId) => {
        url = url.concat(`&tags=${tagId}`);
      });
      return url;
    }
    if (websocket) {
      return ({
        listen: (type: string, action: (message: string) => void) => {
          try {
            if (websocket) {
              if (type === 'message') {
                messageAction = action;
              }
              if (type === 'ping') {
                pingAction = action;
              }
              if (type === 'data') {
                dataAction = action;
              }
              if (type === 'error') {
                errorAction = action;
              }
              websocket.onmessage = ((event: any) => {
                if (event.type === 'message' && messageAction) {
                  messageAction(event.data);
                }
                if (event.type === 'ping' && pingAction) {
                  pingAction(event.data);
                }
                if (event.type === 'data' && dataAction) {
                  dataAction(event.data);
                }
                if (event.type === 'error' && errorAction) {
                  errorAction(event.data);
                }
                return event.data;
              });
            } else {
              throw new Error(`No websocket connection, websocket data: ${websocket}`);
            }
          } catch (e) {
            // handle error
            throw e;
          }
        },
      });
    }
    return null;
  },
});
