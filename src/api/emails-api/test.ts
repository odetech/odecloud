import { expect } from 'chai';
import { Emails } from './model';

describe('Email\'s Model', () => {
  const MockEmails = () => new Emails({
    subject: '123',
    template: 'Hello World',
    data: {
      description: 'Lorem ipsum dolor sit amet',
    },
    recipient_emails: ['String'],
  });

  it('checks basic', () => {
    const emails = MockEmails();

    expect(emails.subject).to.equal('123');
    expect(emails.template).to.equal('Hello World');
    expect(emails.data.description).to.equal('Lorem ipsum dolor sit amet');
    expect(emails.recipient_emails[0]).to.equal('String');
  });

  describe('payload()', () => {
    const emails = MockEmails();

    it('checks getEmailsPostPayload()', () => {
      const payload = emails.payload().getEmailsPostPayload({
        recipient_emails: ['test@email.com']
      });

      expect(payload.subject).to.equal('123');
      expect(payload.template).to.equal('Hello World');
      // expect(payload.data.description).to.equal('Lorem ipsum dolor sit amet');
      expect(payload.recipient_emails[0]).to.equal('test@email.com');
    });
  });
});
