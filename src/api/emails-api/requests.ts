import {
  post,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  sendEmail: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('emails'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
