import { requests } from './requests';
import { Emails } from './model';
import type EmailsTypes from './types';

const emails = (getRequestOptions: any, getEndpoint: any) => ({
  model: Emails,
  payload: { ...new Emails({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  EmailsTypes,
};

export default emails;
