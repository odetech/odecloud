import EmailsTypes from './types';

export class Emails implements EmailsTypes {
  subject = '';
  template = '';
  data = {
    description: '',
  };
  recipient_emails = [];

  constructor(data: any) {
    if (!data) {
      return;
    }

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getEmailsPostPayload: (patchPayload?: any) => {
      const payload: {
        subject: string,
        template: string,
        data?: any,
        recipient_emails?: string[],
      } = {
        subject: this.subject,
        template: this.template,
      };

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
  });
}
