type EmailsTypes = {
  subject: string,
  template: string,
  data?: {
    description: any,
  },
  recipient_emails?: string[],
};

export default EmailsTypes;
