type NetworkTypes = {
  _id: string | null,
  isPending: boolean | null,
  userId1: number | null | string,
  userId2: number | null | string,
  createdAt: null | Date,
  updatedAt: null | Date,
};

export default NetworkTypes;
