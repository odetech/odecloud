import NetworkTypes from './types';

export class Network implements NetworkTypes {
  _id = '';
  isPending = false;
  userId1 = 0; // Master user's ID
  userId2 = 0;
  createdAt = null;
  updatedAt = null;

  constructor(data: any) {
    if (!data) {
      return;
    }

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getJoinInvitationsPayload: (payloadData: {
      emails: string[],
      subject: string,
      message: string,
    }) => {
      const payload = {
        emails: payloadData.emails,
        subject: payloadData.subject,
        message: payloadData.message,
      };

      return payload;
    },

    getAcceptInvitationPayload: (payloadData: {
      isPending?: boolean,
      userId1: string,
      userId2: string,
      updatedAt?: Date,
      user2?: unknown,
    }) => {
      const payload: {
        userId1: string,
        userId2: string,
      } = {
        userId1: payloadData.userId1,
        userId2: payloadData.userId2,
      };

      return payload;
    },
  });
}
