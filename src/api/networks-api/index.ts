import { requests } from './requests';
import { Network } from './model';
import type NetworkTypes from './types';

const networks = (getRequestOptions: any, getEndpoint: any) => ({
  model: Network,
  payload: { ...new Network({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  NetworkTypes,
};

export default networks;
