import { expect } from 'chai';
import { Network } from './model';

describe('Network\'s Model', () => {
  const MockNetwork = () => new Network({
    '_id' : 'DtpAnwgoTnt57g9ht',
    'userId1' : '123',
    'userId2' : '456',
    'isPending': true,
    'createdAt' : '2018-12-25T21:26:20.036Z',
  });

  it('checks basic', () => {
    const network = MockNetwork();

    expect(network._id).to.equal('DtpAnwgoTnt57g9ht');
    expect(network.userId1).to.equal('123');
    expect(network.userId2).to.equal('456');
    expect(network.isPending).to.equal(true);
    expect(network.updatedAt).to.equal(null);
    expect(new Date(network.createdAt).getFullYear()).to.equal(2018);
  });

  describe('payload()', () => {
    const network = MockNetwork();

    it('checks getJoinInvitationsPayload()', () => {
      const payload = network.payload().getJoinInvitationsPayload({
        emails: ['vanielle@odecloud.com'],
        subject: 'asd',
        message: 'qwe',
      });
      expect(payload.emails[0]).to.equal('vanielle@odecloud.com');
      expect(payload.subject).to.equal('asd');
      expect(payload.message).to.equal('qwe');
    });

    it('checks getAcceptInvitationPayload()', () => {
      const payload = network.payload().getAcceptInvitationPayload({
        userId1: 'qwe',
        userId2: 'asd',
      });
      expect(payload.userId1).to.equal('qwe');
      expect(payload.userId2).to.equal('asd');
    });
  });
});
