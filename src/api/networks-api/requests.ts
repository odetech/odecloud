import { deleteRequest, get, post } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getNetworks: async (searchParams?: {
    userId1?: string,
    userId2?: string,
    isPending?: boolean,
    returnUser2?: boolean,
    exclude?: string[],
    pageNumber?: number,
    createdBy?: string,
    updatedBy?: string,
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.exclude) {
      delete searchParamsCopy.exclude;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.exclude) {
      searchParams.exclude.forEach((item) => {
        searchParamsString.append('exclude', item);
      });
    }

    let url = getEndpoint('networks');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }

    try {
      const requestData = await get(
        getRequestOptions,
        url,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  invite: async (payload: {
    inviteEmail?: string,
    userId?: string,
  }) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('networks/invite'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  joinInvitations: async (payload: {
    emails: string[],
    subject: string,
    message: string,
  }) => {
    try {
      const requestData = await post(
        getRequestOptions,
        getEndpoint('networks/join-invitations'),
        payload,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  acceptInvitation: async (payload: {
    userId1: string,
    userId2: string,
    _id: string,
  }) => {
    try {
      const requestData = await post(
        getRequestOptions,
        getEndpoint('networks/accept'),
        payload,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  count: async (payload: {
    userId1: string,
  }) => {
    try {
      const requestData = await get(
        getRequestOptions,
        getEndpoint('networks/count'),
        payload,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getConnections: async (payload?: {
    userId1: string,
  }) => {
    try {
      const requestData = await get(
        getRequestOptions,
        getEndpoint('networks/connections'),
        payload,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (networkId: string) => {
    try {
      const requestData = await deleteRequest(
        getRequestOptions,
        getEndpoint(`networks/${networkId}`),
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
