import { expect } from 'chai';
import { Questionnaire } from './model';

describe('Questionnaire\'s Model', () => {
  const MockQuestionnaire = () => new Questionnaire({
    _id: 'DtpAnwgoTnt57g9ht',
    title: '123',
    where: '456',
    questions: [{
      _id: 'afsdfasfd',
      title: 'How is the weather today?',
      type: 'rating',
      multiple: null,
      options: [],
    }],
  });

  it('checks basic', () => {
    const questionnaire = MockQuestionnaire();

    expect(questionnaire._id).to.equal('DtpAnwgoTnt57g9ht');
    expect(questionnaire.title).to.equal('123');
    expect(questionnaire.where).to.equal('456');
  });

  describe('payload()', () => {
    const questionnaire = MockQuestionnaire();

    it('checks getPostPayload()', () => {
      const payload = questionnaire.payload().getPostPayload({
        questions: [
          {
            _id: '444555666',
            title: 'How are you?',
            type: 'rating',
            multiple: null,
            options: [],
          }
        ],
        where: '888777',
      });

      expect(payload.questions[0].type).to.equal('rating');
      expect(payload.where).to.equal('888777');
      expect(payload.title).to.equal('123');
    });

    it('checks getPatchPayload()', () => {
      const payload = questionnaire.payload().getPatchPayload({
        questions: [
          {
            _id: '555',
            title: 'How well do you know js?',
            type: 'rating',
            multiple: null,
            options: [],
          }
        ],
        where: '987654',
      });

      expect(payload.questions[0]._id).to.equal('555');
      expect(payload.where).to.equal('987654');
    });
  });
});
