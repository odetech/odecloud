import {post, patch, get} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getQuestionnaires: async (searchParams?: {
    where?: string,
    status?: string | 'draft' | 'published' | 'archived',
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
    data?: string[],
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }
    let url = getEndpoint('questionnaires');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString().replace(/%3A/gm,':')}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (elementId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`questionnaires/${elementId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: {
    questions: any,
    title: string,
    where: string,
  }) => {
    try {
      const requestData = await post(
        getRequestOptions,
        getEndpoint('questionnaires'),
        payload,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
