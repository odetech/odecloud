import { requests } from './requests';
import { Questionnaire } from './model';
import type QuestionnaireTypes from './types';

const questionnaires = (getRequestOptions: any, getEndpoint: any) => ({
  model: Questionnaire,
  payload: { ...new Questionnaire({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  QuestionnaireTypes,
};

export default questionnaires;
