import QuestionnaireTypes from './types';

export class Questionnaire implements QuestionnaireTypes {
  _id = '';
  title = '';
  where = '';
  status = 'draft';
  questions = [];
  updatedBy = '';
  updatedAt = null;

  constructor(data: any) {
    if (!data) {
      return;
    }

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPatchPayload: (payloadData: {
      title?: string,
      where?: string,
      questions?: any,
    }) => {
      const payload = {
        title: payloadData.title || this.title,
        where: payloadData.where || this.where,
        questions: payloadData.questions || this.questions,
      };

      return payload;
    },

    getPostPayload: (payloadData: {
      title?: string,
      where?: string,
      questions?: any,
    }) => {
      const payload = {
        title: payloadData.title || this.title,
        where: payloadData.where || this.where,
        questions: payloadData.questions || this.questions,
      };

      return payload;
    },
  });
}
