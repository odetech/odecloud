type QuestionnaireTypes = {
  _id: string,
  title: string | null,
  where: string | null,
  status: string | 'draft' | 'published' | 'archived' | null,
  questions: any,
  updatedBy: string | null,
  updatedAt: null | Date,
};

export default QuestionnaireTypes;
