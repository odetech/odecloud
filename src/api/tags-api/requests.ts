import { get, patch, post } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getFeeds: async (searchParams?: {
    data?: string[],
    messagesData?: string[],
    associatedId?: string,
    messagesAssociated_id?: string,
    isSystemDefault?: boolean,
    isPublic?: boolean,
    title?: string,
    isArchive?: boolean,
    isDirectChat?: string,
    members?: string[],
    createdBy?: string,
    sortBy?: string,
    updatedBy?: string,
    messagesLoadBy?: string[],
    limit?: number,
    messagesLimit?: number,
    messagesPageNumber?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    if (searchParamsCopy.members) {
      delete searchParamsCopy.members;
    }

    if (searchParamsCopy.messagesData) {
      delete searchParamsCopy.messagesData;
    }

    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }
    if (searchParams?.messagesData) {
      searchParams.messagesData.forEach((item) => {
        searchParamsString.append('messagesData', item);
      });
    }
    if (searchParams?.members) {
      searchParams.members.forEach((item) => {
        searchParamsString.append('members', item);
      });
    }
    if (searchParams?.messagesLoadBy) {
      searchParams.messagesLoadBy.forEach((item) => {
        searchParamsString.append('messagesLoadBy', item);
      });
    }

    let url = getEndpoint('tags');
    if (searchParams) {
      url = url.concat(`/feeds?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getTags: async (searchParams?: {
    data?: string[],
    associatedId?: string,
    isSystemDefault?: boolean,
    isPublic?: boolean,
    isArchive?: boolean,
    members?: string[],
    createdBy?: string,
    updatedBy?: string,
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    if (searchParamsCopy.members) {
      delete searchParamsCopy.members;
    }

    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }
    if (searchParams?.members) {
      searchParams.members.forEach((item) => {
        searchParamsString.append('members', item);
      });
    }

    let url = getEndpoint('tags');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getTag: async (tagId: string, searchParams?: {
    data?: string[],
    associatedId?: string,
    isSystemDefault?: boolean,
    isPublic?: boolean,
    isArchive?: boolean,
    members?: string[],
    createdBy?: string,
    updatedBy?: string,
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    if (searchParamsCopy.members) {
      delete searchParamsCopy.members;
    }

    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }
    if (searchParams?.members) {
      searchParams.members.forEach((item) => {
        searchParamsString.append('members', item);
      });
    }

    let url = getEndpoint(`tags/${tagId}`);
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getChatRooms: async (searchParams?: {
    cache?: boolean,
    data?: string[],
    associatedId?: string,
    isSystemDefault?: boolean,
    isArchive?: boolean,
    isPublic?: boolean,
    isDirectChat?: boolean,
    members?: string[],
    pageNumber?: number,
    messagesLimit?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    if (searchParamsCopy.members) {
      delete searchParamsCopy.members;
    }

    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }
    if (searchParams?.members) {
      searchParams.members.forEach((item) => {
        searchParamsString.append('members', item);
      });
    }

    let url = getEndpoint('tags/chats');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getChatRoom: async (tagId: string, searchParams?: {
    cache?: boolean,
    data?: string[],
    associatedId?: string,
    isSystemDefault?: boolean,
    isArchive?: boolean,
    isPublic?: boolean,
    isDirectChat?: boolean,
    members?: string[],
    pageNumber?: number,
    messagesLimit?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }
    if (searchParamsCopy.members) {
      delete searchParamsCopy.members;
    }

    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }
    if (searchParams?.members) {
      searchParams.members.forEach((item) => {
        searchParamsString.append('members', item);
      });
    }

    let url = getEndpoint(`tags/chats/${tagId}`);
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const request = await get(
        getRequestOptions,
        url,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint('tags'),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (tagId: string, payload: any) => {
    try {
      const request = await patch(
        getRequestOptions,
        getEndpoint(`tags/${tagId}`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  sendChatMessage: async (tagId: string, payload: any) => {
    try {
      const request = await post(
        getRequestOptions,
        getEndpoint(`tags/${tagId}/push`),
        payload,
      );
      return request;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
