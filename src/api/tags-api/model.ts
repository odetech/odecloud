import { BasicModel } from '../basicModel';
import TagTypes from './types';

export class Tag extends BasicModel implements TagTypes {
  _id = '';
  title = '';
  associatedId = '';
  isSystemDefault = false;
  isPublic = false;
  isDirectChat = false;
  isArchive = false;
  members = [];
  owner = '';

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: () => {
      const payload = {
        title: this.title,
        associatedId: this.associatedId,
        isSystemDefault: this.isSystemDefault,
        isPublic: this.isPublic,
        isArchive: this.isArchive,
        isDirectChat: this.isDirectChat,
        members: this.members,
        owner: this.owner,
        createdBy: this.createdBy,
        updatedBy: this.updatedBy,
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      return payload;
    },
    getPatchPayload: () => {
      const payload: {
        isArchive: boolean,
        updatedAt: Date,
        title?: string,
        associatedId?: string,
        isSystemDefault?: boolean,
        isPublic?: boolean,
        members?: string[],
        owner?: string,
      } = {
        isArchive: this.isArchive,
        updatedAt: new Date(),
      };

      if (this.title !== '') {
        payload.title = this.title;
      }

      return payload;
    },
    getSendChatMessagePayload: (chatMessagePayload: any) => {
      const payload: {
        content: string,
        rawContent: string,
        createdBy?: string,
        updatedBy?: string,
      } = {
        content: '',
        rawContent: '',
      };

      const keys = Object.keys(chatMessagePayload);
      keys.forEach((key: string) => {
        if (chatMessagePayload[key] !== undefined) {
          payload[key] = chatMessagePayload[key];
        }
      });

      return payload;
    },
  });
}
