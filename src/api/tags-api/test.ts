import { expect } from 'chai';
import { Tag } from './model';

describe('Tag\'s Model', () => {
  const MockTag = () => new Tag({
    _id: '123456',
    title: 'Computer Science',
    associatedId: 'XXXXXXX',
    isSystemDefault: false,
    isPublic: false,
    isArchive: false,
    isDirectChat: false,
    members: ['you'],
    owner: 'Someone',
  });

  it('checks basic', () => {
    const tag = MockTag();
    tag.setCurrentUserId('YYY')
    expect(tag._id).to.equal('123456');
    expect(tag.title).to.equal('Computer Science');
    expect(tag.associatedId).to.equal('XXXXXXX');
    expect(tag.isSystemDefault).to.equal(false);
    expect(tag.isPublic).to.equal(false);
    expect(tag.isDirectChat).to.equal(false);
    expect(tag.isArchive).to.equal(false);
    expect(tag.members.length).to.equal(1);
    expect(tag.owner).to.equal('Someone');
    expect(tag.updatedBy).to.equal('YYY');
    expect(tag.createdBy).to.equal('YYY');
  });

  describe('payload()', () => {
    const tag = MockTag();
    tag.setCurrentUserId('YYY');

    it('checks getPostPayload()', () => {
      const payload = tag.payload().getPostPayload();
      expect(payload.title).to.equal(tag.title);
      expect(payload.associatedId).to.equal(tag.associatedId);
      expect(payload.isSystemDefault).to.equal(tag.isSystemDefault);
      expect(payload.isPublic).to.equal(tag.isPublic);
      expect(payload.isArchive).to.equal(tag.isArchive);
      expect(payload.members[0]).to.equal('you');
      expect(payload.owner).to.equal(tag.owner);
      expect(new Date(payload.createdAt).getTime() > new Date(tag.createdAt).getTime()).to.equal(true);
      expect(new Date(payload.updatedAt).getTime() > new Date(tag.updatedAt).getTime()).to.equal(true);
      expect(payload.updatedBy).to.equal(tag.updatedBy);
      expect(payload.createdBy).to.equal(tag.createdBy);
    });

    it('checks getPatchPayload()', () => {
      const payload = tag.payload().getPatchPayload();
      expect(payload.title).to.equal(tag.title);
      expect(payload.isArchive).to.equal(tag.isArchive);
    });

    it('checks getSendChatMessagePayload()', () => {
      const payload = tag.payload().getSendChatMessagePayload({
        content: 'asd',
        rawContent: 'qwe',
      });
      expect(payload.content).to.equal('asd');
      expect(payload.rawContent).to.equal('qwe');
    });

  });
});
