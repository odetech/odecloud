import { requests } from './requests';
import { Tag } from './model';
import type TagTypes from './types';

const tags = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Tag,
    payload: { ...new Tag({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  TagTypes,
};

export default tags;
