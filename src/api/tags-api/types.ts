type TagTypes = {
  _id: string,
  title: string,
  associatedId?: string,
  isSystemDefault?: boolean,
  isPublic?: boolean,
  isArchive?: boolean,
  members: string[],
  owner?: string,
};

export default TagTypes;
