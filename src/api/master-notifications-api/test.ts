import { expect } from 'chai';
import { MasterNotifications } from './model';

describe('Master Notifications Model', () => {
  const MockMasterNotifications = () => new MasterNotifications({
    app: 'odetask',
    frequencies: ['aa'],
    frequency: 'aa',
  });

  it('checks basic', () => {
    const masterNotifications = MockMasterNotifications();

    expect(masterNotifications.app).to.equal('odetask');
    expect(masterNotifications.frequency).to.equal('aa');
    expect(masterNotifications.frequencies[0]).to.equal('aa');
  });

  describe('payload()', () => {
    const masterNotifications = MockMasterNotifications();

    it('checks getFrequenciesPatchPayload()', () => {
      const payload = masterNotifications.payload().getFrequenciesPatchPayload();

      expect(payload.frequencies[0]).to.equal(masterNotifications.frequencies[0]);
      expect(payload.app).to.equal(masterNotifications.app);
    });

    it('checks getMuteOptionsPatchPayload()', () => {
      const payload = masterNotifications.payload().getMuteOptionsPatchPayload();

      expect(payload.app).to.equal(masterNotifications.app);
      expect(payload.frequency).to.equal(masterNotifications.frequency);
      // expect(new Date(payload.endAt).getTime() > new Date(masterNotifications.endAt).getTime()).to.equal(true);
    });
  });
});
