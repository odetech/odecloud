import { BasicModel } from '../basicModel';
import MasterNotificationsTypes from './types';

export class MasterNotifications extends BasicModel implements MasterNotificationsTypes {
  app = '';
  frequencies = [];
  frequency = '';
  endAt: '';

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => {
    return ({
      getFrequenciesPatchPayload: () => {
        const payload = {
          frequencies: this.frequencies,
          app: this.app,
        };

        return payload;
      },
      getMuteOptionsPatchPayload: () => {
        const payload = {
          frequency: this.frequency,
          app: this.app,
          endAt: new Date(),
        };

        return payload;
      },
    });
  };
}
