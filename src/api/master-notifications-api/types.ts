type MasterNotificationsTypes = {
  app: string,
  frequencies: string[],
  frequency: string,
  endAt?: string | null,
};

export default MasterNotificationsTypes;
