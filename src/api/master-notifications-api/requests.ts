import { patch } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  patchFrequencies: async (masterNotificationsId: string, payload: any) => {
    try {
      const resultId = await patch(
        getRequestOptions,
        getEndpoint(`master-notifications/frequencies`),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
  patchMuteOptions: async (masterNotificationsId: string, payload: any) => {
    try {
      const resultId = await patch(
        getRequestOptions,
        getEndpoint(`master-notifications/${masterNotificationsId}/mute-options`),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e
    }
  },
});
