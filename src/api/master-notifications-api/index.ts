import { requests } from './requests';
import { MasterNotifications } from './model';
import type MasterNotificationsTypes from './types';

const masterNotifications = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: MasterNotifications,
    payload: { ...new MasterNotifications({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  MasterNotificationsTypes,
};

export default masterNotifications;
