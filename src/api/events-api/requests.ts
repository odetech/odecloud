import {
  get, patch, post, deleteRequest,
} from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getEvents: async (searchParams?: {
    data?: string[],
    isByStaff?: boolean,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }

    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint('events');
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const requestData = await get(
        getRequestOptions,
        url,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getEvent: async (eventId: string, searchParams?: {
    data?: string[],
    isByStaff?: boolean,
    pageNumber?: number,
    limit?: number,
    createdBy?: string,
    updatedBy?: string,
    isStaff?: number,
    isDev?: number,
  }) => {
    const searchParamsCopy = searchParams ? JSON.parse(JSON.stringify(searchParams)) : {};
    if (searchParamsCopy.data) {
      delete searchParamsCopy.data;
    }

    const searchParamsString = new URLSearchParams(searchParamsCopy);
    if (searchParams?.data) {
      searchParams.data.forEach((item) => {
        searchParamsString.append('data', item);
      });
    }

    let url = getEndpoint(`events/${eventId}`);
    if (searchParams) {
      url = url.concat(`/?${searchParamsString.toString()}`);
    }
    try {
      const requestData = await get(
        getRequestOptions,
        url,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: {
    title: string,
    url: string,
    description?: string,
    tags?: string[],
    hostUserId: string,
    startDate: Date,
    endDate: Date,
    attendees: string[],
    createdBy?: string,
    updatedBy?: string,
    isByStaff?: boolean,
  }) => {
    try {
      const requestData = await post(
        getRequestOptions,
        getEndpoint('events'),
        payload,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  patch: async (eventId: string, payload: {
    title?: string,
    url?: string,
    description?: string,
    tags?: string[],
    hostUserId?: string,
    startDate?: Date,
    endDate?: Date,
    attendees?: string[],
    createdBy?: string,
    updatedBy?: string,
    isByStaff?: boolean,
  }) => {
    try {
      const requestData = await patch(
        getRequestOptions,
        getEndpoint(`events/${eventId}`),
        payload,
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (eventId: string) => {
    try {
      const requestData = await deleteRequest(
        getRequestOptions,
        getEndpoint(`events/${eventId}`),
      );
      return requestData;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
