import { BasicModel } from '../basicModel';
import EventTypes from './types';

export class Event extends BasicModel implements EventTypes {
  _id = '';
  title = '';
  description = '';
  hostUserId = '';
  startDate = new Date();
  endDate = new Date();
  attendees = [];
  tags = [];
  data = [];
  isByStaff = false;

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: () => {
      const payload = {
        title: this.title,
        url: this.description,
        description: this.description,
        tags: this.tags,
        hostUserId: this.hostUserId,
        startDate: this.startDate !== null ? new Date(this.startDate || '') : null,
        endDate: this.endDate !== null ? new Date(this.endDate || '') : null,
        attendees: this.attendees,
        createdBy: this.createdBy,
        updatedBy: this.updatedBy,
        isByStaff: this.isByStaff,
        data: this.data,
      };

      return payload;
    },

    getPatchPayload: (patchPayload?: any) => {
      const payload: {
        title?: string,
        url?: string,
        description?: string,
        tags?: string[],
        hostUserId?: string,
        startDate?: Date,
        endDate?: Date,
        attendees?: string[],
        createdBy?: string,
        updatedBy?: string,
        isByStaff?: boolean,
        data?: string[],
      } = {};

      const keys = Object.keys(patchPayload);
      keys.forEach((key: string) => {
        if (patchPayload[key] !== undefined) {
          payload[key] = patchPayload[key];
        }
      });

      return payload;
    },
  });
}
