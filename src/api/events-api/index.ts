import { requests } from './requests';
import { Event } from './model';
import type EventTypes from './types';

const events = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Event,
    payload: { ...new Event({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  EventTypes,
};

export default events;
