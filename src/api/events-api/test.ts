import { expect } from 'chai';
import { Event } from './model';

describe('Event\'s Model', () => {
  const MockEvent = () => new Event({
    _id: 'DtpAnwgoTnt57g9ht',
    title: '123ABC',
    description: 'Lorem Ipsum is',
    hostUserId: '123XYZ',
    isByStaff: false,
    startDate: new Date('2018-12-25T21:26:20.036Z'),
    endDate: new Date('2018-12-25T21:26:20.036Z'),
    attendees: ['123', 'ABC', 'XYZ'],
    createdBy: 'ustt27cu6BdA5GMRY',
    updatedBy: 'ustt27cu6BdA5GMRY',
  });

  it('checks basic', () => {
    const event = MockEvent();

    expect(event._id).to.equal('DtpAnwgoTnt57g9ht');
    expect(event.title).to.equal('123ABC');
    expect(event.description).to.equal('Lorem Ipsum is');
    expect(event.hostUserId).to.equal('123XYZ');
    expect(event.isByStaff).to.equal(false);
    expect(new Date(event.startDate).getFullYear()).to.equal(2018);
    expect(new Date(event.endDate).getFullYear()).to.equal(2018);
    expect(event.attendees.length).to.equal(3);
    expect(event.createdBy).to.equal('ustt27cu6BdA5GMRY');
    expect(event.updatedBy).to.equal('ustt27cu6BdA5GMRY');
  });

  describe('payload()', () => {
    const event = MockEvent();

    it('checks getPostPayload()', () => {
      const payload = event.payload().getPostPayload();

      expect(payload.title).to.equal(event.title);
      expect(payload.description).to.equal(event.description);
      expect(payload.hostUserId).to.equal(event.hostUserId);
      expect(payload.isByStaff).to.equal(event.isByStaff);
      expect(payload.startDate > new Date(event.startDate)).to.equal(false);
      expect(payload.endDate > new Date(event.endDate)).to.equal(false);
      expect(payload.tags).to.equal(event.tags);
      expect(payload.attendees).to.equal(event.attendees);
      expect(payload.updatedBy).to.equal(event.updatedBy);
      expect(payload.createdBy).to.equal(event.createdBy);
    });

    it('checks getPatchPayload()', () => {
      const payload = event.payload().getPatchPayload({
        title: '123ABC',
        updatedBy: 'ustt27cu6BdA5GMRY',
      });

      expect(payload.title).to.equal(event.title);
      expect(payload.updatedBy).to.equal(event.updatedBy);
    });

  });
});
