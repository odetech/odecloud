type EventTypes = {
  _id: string,
  title?: string,
  url?: string,
  description?: string,
  tags?: string[],
  hostUserId?: string,
  startDate?: Date,
  endDate?: Date,
  attendees?: string[],
  createdBy?: string,
  updatedBy?: string,
  isByStaff?: boolean,
  updatedAt?: Date,
  data?: string[],
};

export default EventTypes;
