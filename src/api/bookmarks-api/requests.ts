import { get, post, deleteRequest } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getBookmarks: async (searchParams?: {
    userId?: string,
    title?: string,
    url?: string,
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const bookmarks = await get(
        getRequestOptions,
        getEndpoint('bookmarks'),
        searchParams,
      );
      return bookmarks;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getBookmark: async (bookmarkId: string, searchParams?: {
    userId?: string,
    title?: string,
    url?: string,
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const bookmark = await get(
        getRequestOptions,
        getEndpoint(`bookmarks/${bookmarkId}`),
        searchParams,
      );
      return bookmark;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('bookmarks'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (bookmarkId: string) => {
    try {
      const resultId = await deleteRequest(
        getRequestOptions,
        getEndpoint(`bookmarks/${bookmarkId}`),
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
});
