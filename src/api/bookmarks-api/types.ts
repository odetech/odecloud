type BookmarkTypes = {
  _id: string,
  userId: string,
  title: string,
  url: string,
};

export default BookmarkTypes;
