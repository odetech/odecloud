import { assert, expect } from 'chai';
import { Bookmark } from './model';

describe('Bookmark\'s Model', () => {
  const MockBookmark = () => new Bookmark({
    _id: '123456',
    title: 'Example Bookmark',
    url: '/1/2/3/4',
    userId: '123',
  });

  it('checks basic', () => {
    const bookmark = MockBookmark();

    expect(bookmark._id).to.equal('123456');
    expect(bookmark.title).to.equal('Example Bookmark');
    expect(bookmark.url).to.equal('/1/2/3/4');
    expect(bookmark.userId).to.equal('123');
  });

  describe('payload()', () => {
    const bookmark = MockBookmark();

    describe('it checks getPostPayload()', () => {
      it('checks basic', () => {
        const payload = bookmark.payload().getPostPayload();
  
        expect(payload.title).to.equal(bookmark.title);
        expect(payload.userId).to.equal(bookmark.userId);
        expect(payload.url).to.equal(bookmark.url);
        expect(new Date(payload.createdAt).getTime() > new Date(bookmark.createdAt).getTime()).to.equal(true);
        expect(new Date(payload.updatedAt).getTime() > new Date(bookmark.updatedAt).getTime()).to.equal(true);
        expect(payload.updatedBy).to.equal(bookmark.updatedBy);
        expect(payload.createdBy).to.equal(bookmark.createdBy);
      });

      it('checks empty \'title\'', () => {
        const bookmark = new Bookmark({ userId: 'xxx', url: 'xxx' });
        expect(() => bookmark.payload().getPostPayload()).to.throw('\'title\' field is required.');
      });

      it('checks empty \'userId\'', () => {
        const bookmark = new Bookmark({ title: 'xxx', url: 'xxx' });
        expect(() => bookmark.payload().getPostPayload()).to.throw('\'userId\' field is required.');
      });

      it('checks empty \'url\'', () => {
        const bookmark = new Bookmark({ title: 'xxx', userId: 'xxx' });
        expect(() => bookmark.payload().getPostPayload()).to.throw('\'url\' field is required.');
      });

    });
  });
});
