import { requests } from './requests';
import { Bookmark } from './model';
import type BookmarkTypes from './types';

const bookmarks = (getRequestOptions: any, getEndpoint: any) => {
  return ({
    model: Bookmark,
    payload: { ...new Bookmark({}).payload() },
    ...requests(getRequestOptions, getEndpoint),
  })
}

export type {
  BookmarkTypes,
};

export default bookmarks;
