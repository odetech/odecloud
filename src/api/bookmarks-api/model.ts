import { BasicModel } from '../basicModel';
import BookmarkTypes from './types';

export class Bookmark extends BasicModel implements BookmarkTypes {
  _id = '';

  userId = '';

  title = '';

  url = '';

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: () => {
      if (!this.title.length) {
        throw new Error('\'title\' field is required.');
      }

      if (!this.userId.length) {
        throw new Error('\'userId\' field is required.');
      }

      if (!this.url.length) {
        throw new Error('\'url\' field is required.');
      }

      const payload = {
        title: this.title,
        userId: this.userId,
        url: this.url,
        createdBy: this.createdBy,
        updatedBy: this.updatedBy,
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      return payload;
    },
  });
}
