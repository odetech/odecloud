import { expect } from 'chai';
import { Answer } from './model';

describe('Answer\'s Model', () => {
  const MockAnswer = () => new Answer({
    _id: 'asd',
    value: 'sdf',
    questionId: 'dfg',
  });

  it('checks basic', () => {
    const answer = MockAnswer();

    expect(answer._id).to.equal('asd');
    expect(answer.value).to.equal('sdf');
    expect(answer.questionId).to.equal('dfg');
  });

  describe('payload()', () => {
    const answer = MockAnswer();

    it('checks getPostPayload()', () => {
      const payload = answer.payload().getPostPayload({
        value: 'sdf',
        questionId: 'dfg',
      });

      expect(payload.value).to.equal('sdf');
      expect(payload.questionId).to.equal('dfg');
    });
  });
});
