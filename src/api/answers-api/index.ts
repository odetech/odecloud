import { requests } from './requests';
import { Answer } from './model';
import type AnswerTypes from './types';

const answers = (getRequestOptions: any, getEndpoint: any) => ({
  model: Answer,
  payload: { ...new Answer({}).payload() },
  ...requests(getRequestOptions, getEndpoint),
});

export type {
  AnswerTypes,
};

export default answers;
