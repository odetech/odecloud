import { deleteRequest, get, post } from '../../base-requests';

export const requests = (getRequestOptions: any, getEndpoint: any) => ({
  getAnswers: async (searchParams?: {
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const answers = await get(
        getRequestOptions,
        getEndpoint('answers'),
        searchParams,
      );
      return answers;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  getAnswer: async (answerId: string, searchParams?: {
    limit?: number,
    isStaff?: number,
    isDev?: number,
  }) => {
    try {
      const answer = await get(
        getRequestOptions,
        getEndpoint(`answers/${answerId}`),
        searchParams,
      );
      return answer;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  post: async (payload: any) => {
    try {
      const resultId = await post(
        getRequestOptions,
        getEndpoint('answers'),
        payload,
      );
      return resultId;
    } catch (e) {
      // handle error
      throw e;
    }
  },
  delete: async (answerId: string) => {
    try {
      const resultId = await deleteRequest(
        getRequestOptions,
        getEndpoint(`answers/${answerId}`),
      );
      return resultId;
    } catch(e) {
      // handle error
      throw e;
    }
  },
});
