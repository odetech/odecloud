import AnswerTypes from './types';
import { BasicModel } from '../basicModel';

export class Answer extends BasicModel implements AnswerTypes {
  _id = '';
  value = '';
  questionId = '';

  constructor(data: any) {
    super(data);

    const keys = Object.keys(this);
    keys.forEach((key: string) => {
      if (data[key] !== undefined) {
        (this as any)[key] = data[key];
      }
    });
  }

  payload = () => ({
    getPostPayload: (postPayload?: AnswerTypes) => {
      const payload = {
        value: postPayload?.value || this.value,
        questionId: postPayload?.questionId || this.questionId,
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      return payload;
    },
  });
}
