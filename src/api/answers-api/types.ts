type AnswerTypes = {
  _id?: string,
  value: string,
  questionId: string;
  createdBy?: string;
  updatedBy?: string,
  createdAt?: Date | null,
  updatedAt?: Date | null,
};

export default AnswerTypes;
