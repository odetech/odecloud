import ky from 'ky';

export default async (getRequestOptions: any, url: string, payload: any, bodyType?: string) => {
  const options = bodyType === 'formData' ? getRequestOptions({}) : getRequestOptions(null);

  if (bodyType === 'formData') {
    options['body'] = payload;
    options['headers'] = {
      ...options.headers,
      'Content-Type': undefined,
    };
  } else {
    options['json'] = payload;
  }

  try {
    const response: any = await ky.post(url, { ...options, timeout: 60000 }).text();
    if (response) {
      return JSON.parse(response);
    }
    return null;
  } catch(e) {
    // handle error
    throw e;
  }
};
