import get from './get';
import post from './post';
import patch from './patch';
import deleteRequest from './delete';
import put from './put';

export {
  get,
  post,
  deleteRequest,
  patch,
  put,
};
