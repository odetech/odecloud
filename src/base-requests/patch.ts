import ky from 'ky';

export default async (getRequestOptions: any, url: string, payload: any) => {
  const options = getRequestOptions(null);
  options['json'] = payload;

  try {
    const response: any = await ky.patch(url, { ...options, timeout: 60000 }).text();
    if (response) {
      return JSON.parse(response);
    }
    return null;
  } catch(e) {
    // handle error
    throw e;
  }
};
