import ky from 'ky';

export default async (getRequestOptions: any, url: string, searchParams?: any, headers?: any) => {
  try {
    const response: any = await ky.get(
      url,
      { ...getRequestOptions(searchParams, headers), timeout: 60000 },
    ).text();
    if (response) {
      return JSON.parse(response);
    }
    return null;
  } catch(e) {
    // handle error
    throw e;
  }
};
