import ky from 'ky';

export default async (getRequestOptions: any, url: string) => {
  const options = getRequestOptions(null);
  try {
    const response: any = await ky.delete(url, { ...options, timeout: 60000 }).text();
    if (response) {
      return JSON.parse(response);
    }
    return null;
  } catch(e) {
    // handle error
    throw e;
  }
};
