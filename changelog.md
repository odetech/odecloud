# @odecloud/odecloud
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.1.71, 2024/10/16
### Added
- N/A

### Fixed
- getTasks should optional field _id

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.65, 2024/06/18
### Added
- Add pathRoles for user

### Fixed
- N/A

### Changed
- Noti logs was renamed to NSPLogs

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.64, 2024/06/18
### Added
- Noti logs API requests

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.63, 2024/04/12
### Added
- Noti logs API requests

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.62, 2024/05/10
### Added
- N/A

### Fixed
- N/A

### Changed
- Networks "invite" request update

### Removed
- Networks "getInvitePayload" method

### Deprecated
- N/A

### Security
- N/A

## 0.1.61, 2024/05/07
### Added
- Tasks export API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.60, 2024/04/12
### Added
- N/A

### Fixed
- Questionnaires API requests

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.59, 2024/04/11
### Added
- Questionnaires API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.57, 2023/11/29
### Added
- Users API:
  - Added getOdeProfilesUsers and getOdeProfilesUser requests

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.56, 2023/11/07
### Added
- N/A

### Fixed
- N/A

### Changed
- Resync get new option 

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.55, 2023/10/27
### Added
- N/A

### Fixed
- Fixed logic related to headers

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.54, 2023/10/27
### Added
- N/A

### Fixed
- Fixed logic related to headers

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.53, 2023/10/26
### Added
- Added logic related to headers

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.52, 2023/10/24
### Added
- Added Externals API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.51, 2023/07/18
### Added
-  N/A

### Fixed
- N/A

### Changed
- Auth resync API

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.49, 2023/07/18
### Added
-  N/A

### Fixed
- N/A

### Changed
- Hashtags API

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.48, 2023/07/18
### Added
-  Hashtags API:

### Fixed
- N/A

### Changed
- Changed getTasks API to get request

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.47, 2023/06/22
### Added
-  Users API:
  - 'patchTimeZone' request added

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.46, 2023/06/03
### Added
- N/A

### Fixed
- Fixed types

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.45, 2023/05/26
### Added
- N/A

### Fixed
- N/A

### Changed
- Changed downloadReports API to get request

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.44, 2023/05/15
### Added
- Added getConverstation API
- Added getMagic API

### Fixed
- Fixed downloadReports API

### Changed
- Package version update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.1.43, 2023/02/17
### Added
- N/A

### Fixed
- N/A

### Changed
- Package version update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.42, 2023/02/17
### Added
- N/A

### Fixed
- N/A

### Changed
- Package version update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.41, 2023/02/17
### Added
- N/A

### Fixed
- N/A

### Changed
- Base requests:
  - Added 'timeout' option

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.40, 2022/12/14
### Added
- N/A

### Fixed
- N/A

### Changed
- Links API:
  - Added 'additionalParams' to 'post' request

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.39, 2022/12/13
### Added
- N/A

### Fixed
- N/A

### Changed
- Messages API:
  - 'patch' request fix
- Networks API:
  - 'patch' request fix

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.38, 2022/12/12
### Added
- N/A

### Fixed
- N/A

### Changed
- Messages API:
  - Added 'origin' parameter to some requests
- Networks API:
  - Added 'origin' parameter to some requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.37, 2022/12/08
### Added
- N/A

### Fixed
- N/A

### Changed
- Messages API:
  - 'patch' request 'additionalParams' update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.36, 2022/11/28
### Added
- N/A

### Fixed
- N/A

### Changed
- Messages API:
  - Added 'additionalParams' to 'patchReadMessages' request

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.35, 2022/11/11
### Added
- Added Expertises API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.34, 2022/10/31
### Added
- Added index.d.ts file

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.33, 2022/10/05
### Added
- N/A

### Fixed
- N/A

### Changed
- Tags API:
  - 'getTags' and 'getTag' request parameters updated

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.32, 2022/10/05
### Added
- N/A

### Fixed
- N/A

### Changed
- Tags API:
  - 'getChatRooms' and 'getChatRoom' request parameters updated

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.31, 2022/09/27
### Added
- Time Trackers API:
  - 'patchEdit' request added

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.30, 2022/09/27
### Added
- Links API:
  - Added 'getLinks' request
  - Added 'getLink' request
  - Added 'post' request
  - Added 'patch' request
- Time Trackers API:
  - 'getReports' request added
  - 'downloadReports' request added
  - 'uploadCsv' request added

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.29, 2022/09/12
### Added
- Files API:
  - Added 'getFiles' request
  - Added 'getFile' request

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.28, 2022/08/04
### Added
- N/A

### Fixed
- N/A

### Changed
- Messages API:
  - 'delete' request update
  - 'patch' request update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.27, 2022/07/13
### Added
- Tasks API:
  - Added _id query for request 'getTasks'
- Projects API:
  - Added _id query for request 'getProjects'

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.26, 2022/07/11
### Added
- Users API:
  - Added _id query for request 'getUsers'

### Fixed
- N/A

### Changed
- Tasks API:
  - Changed GET projectId query for request 'getTasks'

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.25, 2022/06/24
### Added
- Tags API:
  - Added GET request 'getChatRoom' to get single chat room data

### Fixed
- N/A

### Changed
- Tags API:
  - Renamed 'getTagsAndChats' to 'getChatRooms'

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.24, 2022/06/15
### Added
- Messages API:
  - Added GET request to get unread messages
  - Added PATCH request to mark messages as read

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.23, 2022/05/27
### Added
- N/A

### Fixed
- N/A

### Changed
- Messages API:
  - Likes PATCH and DELETE requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.22, 2022/05/11
### Added
- N/A

### Fixed
- Messages API:
  - fixed colon encoding logic in searchParamsString 

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.21, 2022/04/28
### Added
- N/A

### Fixed
- N/A

### Changed
- Messages API:
  - Likes PATCH request

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.20, 2022/04/14
### Added
- Tasks API:
  - Tasks Search request

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.19, 2022/04/12
### Added
- N/A

### Fixed
- N/A

### Changed
- Messages API:
  - Added additionalParams to PATCH requests url

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.18, 2022/04/08
### Added
- N/A

### Fixed
- Messages API:
  - Like requests url fix

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.17, 2022/04/08
### Added
- N/A

### Fixed
- N/A

### Changed
- Messages API:
  - Added 'additionalParams' to POST requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.16, 2022/04/01
### Added
- Tags API:
  - Added getTagsAndChats GET request

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.15, 2022/03/24
### Added
- Messages API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.14, 2022/03/17
### Added
- N/A

### Fixed
- N/A

### Changed
- Comments API:
  - Post request logic update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.13, 2022/03/14
### Added
- N/A

### Fixed
- N/A

### Changed
- Resync logic update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.12, 2022/03/11
### Added
- N/A

### Fixed
- N/A

### Changed
- Auth API:
  - Resync POST request payload update
- Resync logic update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.11, 2022/03/11
### Added
- Auth API:
  - Resync POST request

### Fixed
- N/A

### Changed
- Resync logic update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.10, 2022/03/11
### Added
- Auth API:
  - Forgot Password Request POST request
  - Forgot Password Accept POST request

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.9, 2022/02/24
### Added
- App Stores API:
  - Create App Store POST request

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.8, 2022/02/22
### Added
- Time Trackers API:
  - Time Tracker Delete request

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.7, 2022/02/01
### Added
- Users API:
  - User Avatar Delete request

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.6, 2022/01/26
### Added
- N/A

### Fixed
- POST request 'formData' body logic update

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.5, 2022/01/26
### Added
- N/A

### Fixed
- Users API:
  - Avatar POST request logic update

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.4, 2022/01/25
### Added
- N/A

### Fixed
- N/A

### Changed
- Users API:
  - Added avatar POST request

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.3, 2022/01/17
### Added
- Emails API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.1.2, 2022/01/10
### Added
- N/A

### Fixed
- N/A

### Changed
- Comments API:
  - POST and PATCH with files are added

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.90, 2021/12/20
### Added
- N/A

### Fixed
- N/A

### Changed
- Articles API:
  - POST and PATCH with files are added

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.89, 2021/12/08
### Added
- N/A

### Fixed
- N/A

### Changed
- Events API:
  - 'data' field added to requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.88, 2021/11/17
### Added
- App Stores API

### Fixed
- N/A

### Changed
- Users API:
  - 'appStore' field added to GET requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.87, 2021/11/17
### Added
- N/A

### Fixed
- N/A

### Changed
- Notifications API:
  - 'data' is added to all GET requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.86, 2021/11/12
### Added
- N/A

### Fixed
- N/A

### Changed
- Feed API:
  - 'loadBy' is added to all GET requests
- Notifications API:
  - 'readAll' request is added
- Tags API:
  - 'data' is added to all GET requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.85, 2021/11/03
### Added
- N/A

### Fixed
- N/A

### Changed
- Feed API:
  - 'data' is added to all GET requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.84, 2021/11/03
### Added
- N/A

### Fixed
- N/A

### Changed
- Articles API:
  - 'data' is added to GET requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.83, 2021/11/02
### Added
- Avatars API

### Fixed
- N/A

### Changed
- Feed API:
  - 'data' is added to 'getFeed' request

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.82, 2021/11/01
### Added
- N/A

### Fixed
- N/A

### Changed
- searchers API:
  - 'projectId' is added to GET requests
- time-trackers API:
  - 'data' is added to POST and PATCH payloads

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.81, 2021/10/29
### Added
- N/A

### Fixed
- Base requests return logic

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.79, 2021/10/14
### Added
- N/A

### Fixed
- masterNotifications API:
  - fix routes

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.78, 2021/10/14
### Added
- masterNotifications API

### Fixed
- N/A

### Changed
- users API:
  - 'masterNotifications' is added from GET requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.77, 2021/10/14
### Added
- N/A

### Fixed
- N/A

### Changed
- projects API:
  - 'projectAssignedToUserId' is removed from GET requests
- orgs API:
  - 'projectAssignedToUserId' is added to GET requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.76, 2021/10/13
### Added
- N/A

### Fixed
- N/A

### Changed
- tasks API:
  - 'orderBy', 'sortBy' are added to GET requests
  - 'data' is added to POST and PATCH payloads
- projects API:
  - 'projectAssignedToUserId' is added to GET requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.75, 2021/09/15
### Added
- Users API:
  - Patch request
- Time Trackers API
- Skillsets API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.74, 2021/08/30
### Added
- Auth API:
  - add password update request
- Searches API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.73, 2021/08/27
### Added
- Feed API:
  - add private channels request

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.72, 2021/08/26
### Added
- articles API:
  - mentions:
    - add email notification request

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.71, 2021/07/26
### Added
- N/A

### Fixed
- Returning the location of placement of types

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.70, 2021/07/26
### Added
- Client Contacts API
- Notes API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.69, 2021/07/14
### Added
- N/A

### Fixed
- N/A

### Changed
- websocket API:
  - 'chats' action update
- tags API:
  - 'sendChatMessage' request update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.68, 2021/07/09
### Added
- tags API:
  - add 'sendChatMessage' request

### Fixed
- N/A

### Changed
- websocket API:
  - actions update:
    - 'chats' url update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.67, 2021/07/01
### Added
- N/A

### Fixed
- N/A

### Changed
- comments API:
  - requests update:
    - 'getComments'
    - 'getComment'

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.66, 2021/06/30
### Added
- Websocket API:
  - actions:
    - add 'pingAction' to 'listen'

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.65, 2021/06/29
### Added
- Tags API:
  - add `isDirectChat` field

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A

## 0.0.64, 2021/06/28
### Added
- N/A

### Fixed
- N/A

### Changed
- Helpers:
  - 'authentication' logic update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.63, 2021/06/28
### Added
- API:
  - networks:
    - delete

### Fixed
- N/A

### Changed
- API:
  - networks:
    - 'post' renamed to 'invite'
  - tasks:
    - 'getTasksMetrics' searchParams are updated

- base-requests:
  - removed unnecessary logic in all requests

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.62, 2021/06/23
### Added
- API:
  - tasks
  - orgs
  - projects

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.61, 2021/06/22
### Added
- auth API:
  - requests:
    - passwordlessRequest
    - passwordlessLogin

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.60, 2021/06/18
### Added
- feed API:
  - requests:
    - 'search' request

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.57, 2021/06/17
### Added
- N/A

### Fixed
- N/A

### Changed
- websocket logic:
  - actions:
    - 'listen' action update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.56, 2021/06/16
### Added
- N/A

### Fixed
- N/A

### Changed
- websocket logic:
  - actions:
    - 'chats' action update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.55, 2021/06/14
### Added
- websocket logic:
  - actions

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.54, 2021/06/07
### Added
- Networks API:
  - Add `networks.getConnections({userId1: <userId1>})` to return all networks data of that user

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.53, 2021/05/31
### Added
- N/A

### Fixed
- Networks API:
  - 'count' request

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.52, 2021/05/27
### Added
- N/A

### Fixed
- N/A

### Changed
- Networks API:
  - payloads and requests update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.51, 2021/05/27
### Added
- N/A

### Fixed
- N/A

### Changed
- Networks API:
  - Returning an old POST request to create a network

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.50, 2021/05/26
### Added
- Networks API:
  - Add `networks.count({userId1: <userId1>})` to return total networks of that user
- Events API:
  - Add support for `isByStaff`

### Fixed
- N/A

### Changed
- Networks API:
  - Update to correct handlers

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.49, 2021/05/26
### Added
- N/A

### Fixed
- N/A

### Changed
- Networks API:
  - query params: pageNumber: number

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.48, 2021/05/26
### Added
- N/A

### Fixed
- N/A

### Changed
- Networks API:
  - Requests update:
      - getNetworks
      - joinInvitations
      - acceptInvitation

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.47, 2021/05/24
### Added
- N/A

### Fixed
- N/A

### Changed
- Articles API:
  - `commentsCount` an integer representing how many comments a specific article has specified by OdeServer

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.46, 2021/05/20
### Added
- N/A

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.45, 2021/05/19
### Added
- N/A

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.44, 2021/05/19
### Added
- Feed API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.43, 2021/05/14
### Added
- Feed API

### Fixed
- N/A

### Changed
- Tags API
  - Add PATCH request
  - Add 'isArchive' parameter
- Files API:
  - getPostPayload update
  - test file update
- odecloud.articles.payload().getPostPayload()
  - Remove createdAt and updatedAt since they will be provided by database

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.44, 2021/05/19
### Added
- Feed API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.43, 2021/05/14
### Added
- N/A

### Fixed
- N/A

### Changed
- Tags API
  - Add PATCH request
  - Add 'isArchive' parameter
- Files API:
  - getPostPayload update
  - test file update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.42, 2021/05/12
### Added
- N/A

### Fixed
- N/A

### Changed
- Base requests
  - Add bodyType to POST request

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.41, 2021/05/12
### Added
- N/A

### Fixed
- N/A

### Changed
- Files API:
  - getPostPayload update
  - test file update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.40, 2021/05/11
### Added
- Tags API POST request

### Fixed
- N/A

### Changed
- Tags API requests update:
  - getTags parameters
  - getTag parameters

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.39, 2021/05/4
### Added
- Notification Settings API

### Fixed
- N/A

### Changed
- API requests update:
  - Notifications API

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.38, 2021/05/3
### Added
- N/A

### Fixed
- N/A

### Changed
- API requests and models update:
  - Answers API
  - Articles API
  - Bookmarks API
  - Comments API
  - Events API
  - Questions API
  - Surveys API
  - Tags API

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.37, 2021/04/29
### Added
- Articles API:
  - Add mentions array
- Comments API:
  - Add mentions array

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.36, 2021/04/26
### Added
- N/A

### Fixed
- N/A

### Changed
- Users requests update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.34, 2021/04/22
### Added
- N/A

### Fixed
- N/A

### Changed
- Base requests logic update

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.33, 2021/04/22
### Added
- Auth API:
  - Support for `odecloud.auth.payload.getLoginPayload()`
  - Support for `odecloud.auth.payload.getSignupPayload()`
  - Support for `odecloud.auth.login()`
  - Support for `odecloud.auth.signup()`
- Surveys API:
  - Support for `odecloud.surveys.payload.getPostPayload()`
  - Support for `odecloud.surveys.getSurveys()`
  - Support for `odecloud.surveys.getSurvey()`
  - Support for `odecloud.surveys.post()`
- Questions API:
  - Support for `odecloud.questions.payload.getPostPayload()`
  - Support for `odecloud.questions.getQuestions()`
  - Support for `odecloud.questions.getQuestion()`
  - Support for `odecloud.questions.post()`
  - Support for `odecloud.questions.delete()`
- Answers API:
  - Support for `odecloud.answers.payload.getPostPayload()`
  - Support for `odecloud.answers.getAnswers()`
  - Support for `odecloud.answers.getAnswer()`
  - Support for `odecloud.answers.post()`
  - Support for `odecloud.answers.delete()`

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.32, 2021/04/16
### Added
- Files API:
  - Support for `odecloud.files.payload().getPostPayload()`
  - Support for `odecloud.files.post()`
  - Support for `odecloud.files.delete()`

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.30, 2021/04/15
### Added
- Articles API:
  - Add `articles.files=[]`

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.29, 2021/03/30
### Added
- Articles API:
  - Extend `/api/v1/articles/search` to filter seaarch via `tags` and `createdBy`,
  - Can like article and dislike article.
- Comments API:
  - Can like comment and dislike comment.

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.28, 2021/03/30
### Added
- Types can be imported from @types folder

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.27, 2021/03/26
### Added
- N/A

### Fixed
- Bookmarks exporting

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.25, 2021/03/26
### Added
- Bookmarks API:
  - HTTP:
    - Add `odecloud.bookmarks.get()`
    - Add `odecloud.bookmarks.post()`
    - Add `odecloud.bookmarks.patch()`
    - Add `odecloud.bookmarks.delete()`
  - Payload:
    - Add `odecloud.bookmarks.payload().getPostPayload()`

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.24, 2021/03/25
### Added
- Notifications API:
  - HTTP:
    - Add `odecloud.notifications.get()`
    - Add `odecloud.notifications.post()`
    - Add `odecloud.notifications.patch()`
    - Add `odecloud.notifications.delete()`
  - Payload:
    - Add `odecloud.notifications.payload().getPostPayload()`
    - Add `odecloud.notifications.payload().getPatchPayload()`

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.23, 2021/03/24
### Added
- N/A

### Fixed
- N/A

### Changed
- Articles API:
  - Add `followers` to `new Article()`
- Comments API:
  - Add `rawValue` to `odecloud.comments.payload().getPostPayload()`

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.22, 2021/03/18
### Added
- Tags API:
  - HTTP:
    - Add `odecloud.tags.get()`
    - Add `odecloud.tags.post()`
    - Add `odecloud.tags.patch()`
    - Add `odecloud.tags.delete()`
  - Payload:
    - Add `odecloud.tags.payload().getPostPayload()`
    - Add `odecloud.tags.payload().getPatchPayload()`
    - Add `odecloud.tags.payload().getDeletePayload()`

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.21, 2021/03/18
### Added
- Networks API:
  - Add `getPostPayload()`
  - Add `getPatchPayload()`

### Fixed
- POST, PATCH and DELETE requests response logic

### Changed
- Articles API: `externalArticleUrl` -> `externalUrl`

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.20, 2021/03/17
### Added
- N/A

### Fixed
- POST, PATCH and DELETE requests response logic

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.19, 2021/03/13
### Added
- Support for Comments API

### Fixed
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.18, 2021/03/13
### Added
- N/A

### Fixed
- Resolved bug to send `response['data]`

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.17, 2021/03/12
### Added
- Add resync function

### Fixed
- N/A

### Changed
- Authenticate now returns authToken and userId
- Refactor out api Models logic

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.16, 2021/03/10
### Added
- Add support for Users API
- Add testings for Users API

### Fixed 
- N/A

### Changed
- N/A 

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.15, 2021/03/08
### Added
- Add basic testing to Articles API
- Add basic testing to Events API
- Add basic testing to Networks API
- Add basic testing to Publications API

### Fixed 
- N/A

### Changed
- N/A 

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.14, 2021/03/05
### Added
- N/A

### Fixed 
- N/A

### Changed
- Fix built that was causing an error in the `/dist` adding in `@babel/preset-env`

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.13, 2021/03/05
### Added
- Articles API:
  - [x] `new Article()` model
- Publications API:
  - [x] `new Publication()` model
- Events API:
  - Based on https://odecloud.app/api/v1/events
  - [x] `new Event()` model
  - [x] `odecloud.events.get()` to retrieve event(s)
  - [x] `odecloud.events.patch()` to edit event
  - [x] `odecloud.events.post()` to create new event
  - [x] `odecloud.events.search()` to search for an event based upon specify string.
  - [x] `odecloud.events.delete()` to delete an event
- Networks API:
  - Based on https://odecloud.app/api/v1/networks
  - [x] `new Network()` model
  - [x] `odecloud.networks.get()` to retrieve network(s)
  - [x] `odecloud.networks.patch()` to edit network
  - [x] `odecloud.networks.post()` to create new network
  - [x] `odecloud.networks.search()` to search for an network based upon specify string.
  - [x] `odecloud.networks.delete()` to delete an network

### Fixed 
- N/A

### Changed
- Refactor out Articles API
  - [x] `odecloud.articles.get()`
  - [x] `odecloud.articles.patch()`
  - [x] `odecloud.articles.post()`
  - [x] `odecloud.articles.search()`
  - [x] `odecloud.articles.delete()`
- Refactor out Publications API
  - [x] `odecloud.publications.get()`
  - [x] `odecloud.publications.patch()`
  - [x] `odecloud.publications.post()`
  - [x] `odecloud.publications.search()`
  - [x] `odecloud.publications.delete()`

### Removed
- N/A

### Deprecated
- N/A

### Security
- N/A


## 0.0.12, 2020-12-01
### Added
- Users API:
  - `odecloud.authenticate()` to enable third-party app to login using OdeCloud's credential.
- Articles API:
  - Based on https://odecloud.app/api/v1/articles
  - `odecloud.articles.get()` to retrieve article(s)
  - `odecloud.articles.patch()` to edit article
  - `odecloud.articles.post()` to create new article
  - `odecloud.articles.search()` to search for an article based upon specify string.
  - `odecloud.articles.delete()` to delete an article
- Publications API
  - Based on https://odecloud.app/api/v1/articles
  - `odecloud.publications.get()` to retrieve publication(s)
  - `odecloud.publications.patch()` to edit publication
  - `odecloud.publications.post()` to create new publication
  - `odecloud.publications.search()` to search for a publication based upon 
  specify string.
  - `odecloud.publications.delete()` to delete an article
- Base Method:
  - `odecloud.get()` work same as `Fetch`'s GET
  - `odecloud.post()` work same as `Fetch`'s POST
  - `odecloud.patch()` work same as `Fetch`'s PATCH
  - `odecloud.delete()` work same as `Fetch`'s DELETE
  
### Fixed 
- N/A

### Changed
- N/A

### Removed
- N/A

### Deprecated
- N/A

### Security
- `odecloud._createAuthorizationHeader()` to save `Headers` information to keep the user loggin. Without this authorisation headers, odecloud.app will reject all API request. 
