module.exports = {
    'presets': ['@babel/preset-react', '@babel/preset-env'],
    'plugins': ['@babel/plugin-proposal-class-properties', '@babel/plugin-transform-runtime'],
    'env': {
        'test': {
            'presets': [
                [
                    '@babel/env',
                    {'targets': {'node': 'current'}}
                ]
            ]
        }
    }
}
